%chops continuous recording into event-locked windows
function EMGbyStim = parseEMG(EMG,EventFlags,MEPplotWin,fs)
    EMGraw = EMG;
    EMG = EMGraw( round(.048*fs):end, : );
    TMS = EventFlags(:,2);
    Estim = EventFlags(:,1);

    TmsTTLtmp = find( TMS >  .5* max(TMS));
    EstimTTLtmp = find(Estim > .5 * max(Estim));
    TmsTTL = TmsTTLtmp( [1; (find(diff(TmsTTLtmp) > 1))+1 ] );

    NumStims = length(TmsTTL);
    for StimNum = 1:NumStims
        MEPinds = TmsTTL(StimNum)-MEPplotWin(1):TmsTTL(StimNum)+MEPplotWin(2);
        EMGbyStim(StimNum,:) = EMG(MEPinds);
    end

