function PASEEG_EMGanalysis(SubjectName,NumSess)
% function MeanSEP = PASEEG_MEPanalysis(EMG,EventFlags)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Load subject data

SubjectDir = ['C:\Users\myarossi\Desktop\PAS_EEG\Subject Folders\'];
SaveDir = [SubjectDir,SubjectName,'\EMG Files\',SubjectName,'MEPanalysis'];
fs = 2048;
MEPplotWin = round([ .05*fs .1*fs ]);
MEPplotInds = ((-1*MEPplotWin(1)) : MEPplotWin(2)) ./fs.*1000; % in ms 
MEPfindWin = round([ .01*fs .04*fs ]);
MEPfindInds = MEPplotWin(1) + MEPfindWin;

SEP_EMGbyStim = [];
TMS_EMGbyStim = [];
PAS_EMGbyStim = [];

for SessNum = 1:NumSess
    
    load([SubjectDir,SubjectName,'\EMG Files\SEP',mat2str(SessNum)]);
    SEP_EMGbyStimTmp = PASEEG_DataOrg(EMG,EventFlags,MEPplotWin,fs);
    SEP_EMGbyStim = [SEP_EMGbyStim;SEP_EMGbyStimTmp];
    clear('EMG','EventFlags','Data')
    
    load([SubjectDir,SubjectName,'\EMG Files\TMS',mat2str(SessNum)]);
    TMS_EMGbyStimTmp = PASEEG_DataOrg(EMG,EventFlags,MEPplotWin,fs);
    TMS_EMGbyStim = [TMS_EMGbyStim;TMS_EMGbyStimTmp];
    clear('EMG','EventFlags','Data')
    
    load([SubjectDir,SubjectName,'\EMG Files\PAS',mat2str(SessNum)]);
    PAS_EMGbyStimTmp = PASEEG_DataOrg(EMG,EventFlags,MEPplotWin,fs);
    PAS_EMGbyStim = [PAS_EMGbyStim;PAS_EMGbyStimTmp];
    clear('EMG','EventFlags','Data')
end

if SubjectName(1:2) == 'MS'
TMS_EMGbyStim(1:24,:) = NaN;
end

if SubjectName(1:2) == 'JS'
PAS_EMGbyStim(302:end,:) = NaN;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate MEP amplitude and organise data by trial
MeanSEP = mean(SEP_EMGbyStim);
DataSize = size(SEP_EMGbyStim);
NumTrials = DataSize(1);

for TrialNum = 1:NumTrials        
    TMS_MaxAmp(TrialNum,:) = max(TMS_EMGbyStim(TrialNum,MEPfindInds));
    TMS_MinAmp(TrialNum,:) = min(TMS_EMGbyStim(TrialNum,MEPfindInds));
    TMS_MEPamp(TrialNum,:) = TMS_MaxAmp(TrialNum,:)-TMS_MinAmp(TrialNum,:);

    PAS_EMGbyStim(TrialNum,:) = PAS_EMGbyStim(TrialNum,:) - MeanSEP; %Note here in order to remove the estim artifact the mean SEP is subtracted from the PAS-MEP
    PAS_MaxAmp(TrialNum,:) = max(PAS_EMGbyStim(TrialNum,MEPfindInds));
    PAS_MinAmp(TrialNum,:) = min(PAS_EMGbyStim(TrialNum,MEPfindInds));
    PAS_MEPamp(TrialNum,:) = PAS_MaxAmp(TrialNum,:)-PAS_MinAmp(TrialNum,:);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure         
subplot(2,2,1)
plot(MEPplotInds,TMS_EMGbyStim,'k')
hold on
% line([-20,-20],[min(TMS_MinAmp),max(TMS_MaxAmp)],'Color','r')
% line([0,0],[min(TMS_MinAmp),max(TMS_MaxAmp)],'Color','g')
line([-20,-20],[-.5,.5],'Color','r','LineWidth',3)
line([0,0],[-.5,.5],'Color','g','LineWidth',3)
fill([10,10,40,40],[-.5,.5,.5,-.5],'b','FaceAlpha',.2,'EdgeColor','none')
xlim([-50 100])
ylim([-.5 .5])
title('TMSonly')
ylabel('Voltage (mV)')
xlabel('Time (ms)')

subplot(2,2,3)
if SubjectName(1:2) == 'MS'
    plot(1:NumTrials,TMS_MEPamp,'o')
    x = 25:NumTrials;
    P = polyfit(x,TMS_MEPamp(25:end)',1);
else
    plot(1:NumTrials,TMS_MEPamp,'o')
    x = 1:NumTrials;
    P = polyfit(x,TMS_MEPamp',1);
end
yfit = P(1)*(x)+P(2);
hold on;
plot(x,yfit,'k','LineWidth',3);
text(10,max([TMS_MEPamp;PAS_MEPamp]),['y = ',mat2str(P(1),2),'x + ',mat2str(P(2),2)],'FontWeight','BOLD')
xlim([0 400])
ylim([0 max([TMS_MEPamp;PAS_MEPamp])+(.2*max([TMS_MEPamp;PAS_MEPamp]))])
xlabel('Trial')
ylabel('MEP Amp (mV)')

subplot(2,2,2)
plot(MEPplotInds,PAS_EMGbyStim,'k')
hold on
line([-20,-20],[-.5,.5],'Color','r','LineWidth',3)
line([0,0],[-.5,.5],'Color','g','LineWidth',3)
fill([10,10,40,40],[-.5,.5,.5,-.5],'b','FaceAlpha',.2,'EdgeColor','none')
axis tight   
xlim([-50 100])
ylim([-.5 .5])
title('PAS')
ylabel('Voltage (mV)')
xlabel('Time (ms)')

subplot(2,2,4)
if SubjectName(1:2) == 'JS'
    plot(1:NumTrials,PAS_MEPamp,'o')
    x = 1:300;
    P = polyfit(x,PAS_MEPamp(1:300)',1);
    
else
    plot(1:NumTrials,PAS_MEPamp,'o')
    x = 1:NumTrials;
    P = polyfit(x,PAS_MEPamp',1);
end
yfit = P(1)*(x)+P(2);
hold on;
plot(x,yfit,'k','LineWidth',3);
text(10,max([TMS_MEPamp;PAS_MEPamp]),['y = ',mat2str(P(1),2),'x + ',mat2str(P(2),2)],'FontWeight','BOLD')
xlim([0 400])
ylim([0 max([TMS_MEPamp;PAS_MEPamp])+(.2*max([TMS_MEPamp;PAS_MEPamp]))])
xlabel('Trial')
ylabel('MEP Amp (mV)')


save(SaveDir,'SEP_EMGbyStim','TMS_EMGbyStim','PAS_EMGbyStim','TMS_MEPamp','PAS_MEPamp')





function EMGbyStim = PASEEG_DataOrg(EMG,EventFlags,MEPplotWin,fs)
EMGraw = EMG;
EMG = EMGraw( round(.048*fs):end, : );
TMS = EventFlags(:,2);
Estim = EventFlags(:,1);

TmsTTLtmp = find( TMS >  .5* max(TMS));
EstimTTLtmp = find(Estim > .5 * max(Estim));
TmsTTL = TmsTTLtmp( [1; (find(diff(TmsTTLtmp) > 1))+1 ] );

NumStims = length(TmsTTL);
for StimNum = 1:NumStims
    MEPinds = TmsTTL(StimNum)-MEPplotWin(1):TmsTTL(StimNum)+MEPplotWin(2);
    EMGbyStim(StimNum,:) = EMG(MEPinds);
end






% plot(TMS,'b')
% hold on
% plot(Estim,'r')
% plot(TmsTTLtmp,TMS(TmsTTLtmp),'ob')
% hold on
% plot(TmsTTL,TMS(TmsTTL),'*b','MarkerSize',10)


 

% 
% function EMGbyStim = PASEEG_DataOrg(EMG,EventFlags)
% fs = 2048;
% MEPplotWin = round([ .05*fs .1*fs ]);
% MEPplotInds = ((-1*MEPplotWin(1)) : MEPplotWin(2)) ./fs.*1000; % in ms 
% 
% 
% EMGraw = EMG;
% EMG = EMGraw( round(.048*fs):end, : );
% TMS = EventFlags(:,2);
% Estim = EventFlags(:,1);
% % plot(TMS,'b')
% % hold on
% % plot(Estim,'r')
% 
% TmsTTLtmp = find( TMS >  .5* max(TMS));
% EstimTTLtmp = find(Estim > .5 * max(Estim));
% TmsTTL = TmsTTLtmp( [1; (find(diff(TmsTTLtmp) > 1))+1 ] );
% 
% plot(TmsTTLtmp,TMS(TmsTTLtmp),'ob')
% hold on
% plot(TmsTTL,TMS(TmsTTL),'*b','MarkerSize',10)
% 
% NumStims = length(TmsTTL);
% figure
% for StimNum = 1:NumStims
%     MEPinds = TmsTTL(StimNum)-MEPplotWin(1):TmsTTL(StimNum)+MEPplotWin(2);
%     EMGbyStim(StimNum,:) = EMG(MEPinds)-MeanSEP;
%     MaxAmp(StimNum,:) = max(EMGbyStim(StimNum,:));
%     MinAmp(StimNum,:) = min(EMGbyStim(StimNum,:));
%     MEPamp(StimNum,:) = MaxAmp(StimNum,:)-MinAmp(StimNum,:);
%     plot(MEPplotInds,EMGbyStim(StimNum,:),'k')
%     hold on  
% end
% 
%  line([-20,-20],[min(MinAmp),max(MaxAmp)],'Color','r')
%  line([0,0],[min(MinAmp),max(MaxAmp)],'Color','g')
%  axis tight   
% 
%  figure
%  plot(1:NumStims,MEPamp,'o')
%  
% 
%  MeanSEP = mean(EMGbyStim);
% 
