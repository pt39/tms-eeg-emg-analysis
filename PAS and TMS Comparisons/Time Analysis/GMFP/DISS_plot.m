%% DISS Plot: TEP at Cz---GMFP---DISS
figure
subplot(3,1,1)
plot(data_tms_clean_avg.time,t(15,:))% I rejected M1(13) M2(19) EOG, so Cz is 15 rather than 16
hold on
grid on
plot(data_tms_clean_avg.time,u(15,:),'r')
legend('TMS','PAS')
title('SVA TEP of Cz')
xlabel('Time(s)');
ylabel('Voltage(uv)')
xlim([-0.0098 0.3999])

subplot(3,1,2)
plot(data_tms_clean_avg.time,l)
hold on
grid on
plot(data_tms_clean_avg.time,m,'r')
legend('TMS','PAS')
title('GMFP')
xlabel('Time(s)');
ylabel('Voltage(uv)')

subplot(3,1,3)
MC=SVADI;% changen the name
MC(2,:)=0;
surf(MC) 
shading interp
set(gca,'xtick',[],'ytick',[]) %delete the scale of x- and y-axis
xlim([1005 1844])% size(JSDDI,2)=4096
title('DISS')
xlabel('Time(s)');
% plot(data_tms_clean_avg.time,JSDDI)
% grid on
% ylabel('Voltage(uv)')



%% surf plot
% MC=JSDDI;
% MC(2,:)=0;
% figure
% surf(MC)
% shading interp

