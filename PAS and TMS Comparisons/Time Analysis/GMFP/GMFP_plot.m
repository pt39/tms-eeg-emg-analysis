%% Cz---GMFP
figure;
subplot(2,1,1)
plot(data_tms_clean_avg.time,z(15,:)) 
%%%%%% I rejected M1(13) M2(19) EOG, so Cz is 15 rather than 16
hold on
plot(data_tms_clean_avg.time,r(15,:),'r')
plot(data_tms_clean_avg.time,s(15,:),'g')
grid on
xlabel('time (s)');
ylabel('Amp (uv)');
title('JSD Cz activation')
legend('SEP','TMS','PAS')
xlim([-0.01 0.4])

subplot(2,1,2)
plot(data_tms_clean_avg.time,h)
hold on
plot(data_tms_clean_avg.time,a,'r')
plot(data_tms_clean_avg.time,g,'g')
grid on
xlabel('time (s)');
ylabel('GMFP (uv)');
title('JSD GMFP')
legend('SEP','TMS','PAS')
xlim([-0.01 0.4])


%% GMFP
figure
plot(data_tms_clean_avg.time,h);
hold on
grid on
plot(data_tms_clean_avg.time,a,'r');
plot(data_tms_clean_avg.time,g,'g');
xlabel('time (s)');
ylabel('GMFP (uv)');
legend('SEP','TMS','PAS')
title('JSD GMFP')
xlim([-0.05 0.45])



%% I try to find if the relationship among PAS, TMS and E-Stim is linear
%  mc=u(16,:)-t(16,:);% the difference between PAS and TMS at Cz channel
%  MC=mc-y(16,:); % compare the E-Stim and the above difference
%  [B,A]=butter(4, [0.5 80]/(EEG.srate/2)); %butter filter
%  MC= filtfilt(B,A,double(MC));
%  figure
%  plot(data_tms_clean_avg.time,MC)
%  grid on
%  title('SVA')
%  xlabel('time (s)')
%  ylabel('Amp (uv)')
%  xlim([-0.05 0.55])


