% Create time-locked average

% cfg = [];
% cfg.preproc.demean = 'yes';
% cfg.preproc.baselinewindow = [-0.1 -.001];
% data_tms_clean_avg = ft_timelockanalysis(cfg, data_tms_clean2);% calculate the TEPs 

% GMFP calculation
time = data_tms_clean_avg.time; % Vector representing the time-points
gmfp = sqrt(sum(((data_tms_clean_avg.avg-repmat(mean(data_tms_clean_avg.avg,1),numel(data_tms_clean_avg.label),1)).^2),1)/numel(data_tms_clean_avg.label));

%Plot GMFP
figure;
plot(time, gmfp,'b');
xlabel('time (s)');
ylabel('GMFP (uv)');
