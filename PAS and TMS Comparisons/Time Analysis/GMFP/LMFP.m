%% LMFP in 4 regions: left M1, right M1, left S1, right S1 because TMS stimulate M1 and E-stim stimulate S1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% left M1: c5 c3 c1=== 42 14 43
% right M1: c2 c4 c6=== 44 16 45
% left S1: cp5 cp3 cp1=== 18 46 19 
% right S1: cp2 cp4 cp6===  20 48 21
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% local PAS
u1=u([42 14 43],:);%local pas in left M1
u2=u([44 16 45],:);%local pas in right M1
u3=u([18 46 19],:);%local pas in left S1
u4=u([20 48 21],:);%local pas in right s1

M=mean(u1);%left M1
for i=1:size(u1,1)
    m1(i,:)=u1(i,:)-M(1,:);
end
m1=sqrt(sum(m1.^2)/3);%size(s1,1)=3

M=mean(u2);%right M1
for i=1:size(u2,1)
    m2(i,:)=u2(i,:)-M(1,:);
end
m2=sqrt(sum(m2.^2)/3);%size(s2,1)=3

M=mean(u3);%left s1
for i=1:size(u3,1)
    m3(i,:)=u3(i,:)-M(1,:);
end
m3=sqrt(sum(m3.^2)/3);%size(s2,1)=3

M=mean(u4);%right s1
for i=1:size(u4,1)
    m4(i,:)=u4(i,:)-M(1,:);
end
m4=sqrt(sum(m4.^2)/3);%size(s2,1)=3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% local TMS
t1=t([42 14 43],:);%local pas in left M1
t2=t([44 16 45],:);%local pas in right M1
t3=t([18 46 19],:);%local pas in left S1
t4=t([20 48 21],:);%local pas in right s1

M=mean(t1);%left M1
for i=1:size(t1,1)
    l1(i,:)=t1(i,:)-M(1,:);
end
l1=sqrt(sum(l1.^2)/3);%size(s1,1)=3

M=mean(t2);%right M1
for i=1:size(t2,1)
    l2(i,:)=t2(i,:)-M(1,:);
end
l2=sqrt(sum(l2.^2)/3);%size(s2,1)=3

M=mean(t3);%left s1
for i=1:size(t3,1)
    l3(i,:)=t3(i,:)-M(1,:);
end
l3=sqrt(sum(l3.^2)/3);%size(s2,1)=3

M=mean(t4);%right s1
for i=1:size(t4,1)
    l4(i,:)=t4(i,:)-M(1,:);
end
l4=sqrt(sum(l4.^2)/3);%size(s2,1)=3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% local sep
y1=y([42 14 43],:);%local pas in left M1
y2=y([44 16 45],:);%local pas in right M1
y3=y([18 46 19],:);%local pas in left S1
y4=y([20 48 21],:);%local pas in right s1

M=mean(y1);%left M1
for i=1:size(y1,1)
    n1(i,:)=y1(i,:)-M(1,:);
end
n1=sqrt(sum(n1.^2)/3);%size(s1,1)=3

M=mean(y2);%right M1
for i=1:size(y2,1)
    n2(i,:)=y2(i,:)-M(1,:);
end
n2=sqrt(sum(n2.^2)/3);%size(s2,1)=3

M=mean(y3);%left s1
for i=1:size(y3,1)
    n3(i,:)=y3(i,:)-M(1,:);
end
n3=sqrt(sum(n3.^2)/3);%size(s2,1)=3

M=mean(y4);%right s1
for i=1:size(y4,1)
    n4(i,:)=y4(i,:)-M(1,:);
end
n4=sqrt(sum(n4.^2)/3);%size(s2,1)=3

%% frand average of LMFP in 4 regions: left M1, right M1, left S1, right S1 because TMS stimulate M1 and E-stim stimulate S1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% left M1: c5 c3 c1=== 42 14 43
% right M1: c2 c4 c6=== 44 16 45
% left S1: cp5 cp3 cp1=== 18 46 19 
% right S1: cp2 cp4 cp6===  20 48 21
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
GrandAVGTMSleftM1=(a1+d1+pd1+l1)/4;
GrandAVGTMSrightM1=(a2+d2+pd2+l2)/4;
GrandAVGTMSleftS1=(a3+d3+pd3+l3)/4;
GrandAVGTMSrightS1=(a4+d4+pd4+l4)/4;

GrandAVGPASleftM1=(g1+c1+pc1+m1)/4;
GrandAVGPASrightM1=(g2+c2+pc2+m2)/4;
GrandAVGPASleftS1=(g3+c3+pc3+m3)/4;
GrandAVGPASrightS1=(g4+c4+pc4+m4)/4;

GrandAVGSEPleftM1=(h1+b1+pf1+n1)/4;
GrandAVGSEPrightM1=(h2+b2+pf2+n2)/4;
GrandAVGSEPleftS1=(h3+b3+pf3+n3)/4;
GrandAVGSEPrightS1=(h4+b4+pf4+n4)/4;

% log value of TMS_LMFP_left M1
LOG_LMFP_left_M1_TMS_Early=log(sum(GrandAVGTMSleftM1(1,1020:1323)));
LOG_LMFP_left_M1_TMS_Median=log(sum(GrandAVGTMSleftM1(1,1324:1639)));
LOG_LMFP_left_M1_TMS_Late=log(sum(GrandAVGTMSleftM1(1,1640:1943)));
% log value of PAS_LMFP_left M1
LOG_LMFP_left_M1_PAS_Early=log(sum(GrandAVGPASleftM1(1,1020:1323)));
LOG_LMFP_left_M1_PAS_Median=log(sum(GrandAVGPASleftM1(1,1324:1639)));
LOG_LMFP_left_M1_PAS_Late=log(sum(GrandAVGPASleftM1(1,1640:1943)));
% log value of SEP_LMFP_left M1
LOG_LMFP_left_M1_SEP_Early=log(sum(GrandAVGSEPleftM1(1,1020:1323)));
LOG_LMFP_left_M1_SEP_Median=log(sum(GrandAVGSEPleftM1(1,1324:1639)));
LOG_LMFP_left_M1_SEP_Late=log(sum(GrandAVGSEPleftM1(1,1640:1943)));

% log value of TMS_LMFP_right M1
LOG_LMFP_right_M1_TMS_Early=log(sum(GrandAVGTMSrightM1(1,1020:1323)));
LOG_LMFP_right_M1_TMS_Median=log(sum(GrandAVGTMSrightM1(1,1324:1639)));
LOG_LMFP_right_M1_TMS_Late=log(sum(GrandAVGTMSrightM1(1,1640:1943)));
% log value of PAS_LMFP_right M1
LOG_LMFP_right_M1_PAS_Early=log(sum(GrandAVGPASrightM1(1,1020:1323)));
LOG_LMFP_right_M1_PAS_Median=log(sum(GrandAVGPASrightM1(1,1324:1639)));
LOG_LMFP_right_M1_PAS_Late=log(sum(GrandAVGPASrightM1(1,1640:1943)));
% log value of PAS_LMFP_right M1
LOG_LMFP_right_M1_SEP_Early=log(sum(GrandAVGSEPrightM1(1,1020:1323)));
LOG_LMFP_right_M1_SEP_Median=log(sum(GrandAVGSEPrightM1(1,1324:1639)));
LOG_LMFP_right_M1_SEP_Late=log(sum(GrandAVGSEPrightM1(1,1640:1943)));


% log value of TMS_LMFP_left S1
LOG_LMFP_left_S1_TMS_Early=log(sum(GrandAVGTMSleftS1(1,1020:1323)));
LOG_LMFP_left_S1_TMS_Median=log(sum(GrandAVGTMSleftS1(1,1324:1639)));
LOG_LMFP_left_S1_TMS_Late=log(sum(GrandAVGTMSleftS1(1,1640:1943)));
% log value of PAS_LMFP_left S1
LOG_LMFP_left_S1_PAS_Early=log(sum(GrandAVGPASleftS1(1,1020:1323)));
LOG_LMFP_left_S1_PAS_Median=log(sum(GrandAVGPASleftS1(1,1324:1639)));
LOG_LMFP_left_S1_PAS_Late=log(sum(GrandAVGPASleftS1(1,1640:1943)));
% log value of SEP_LMFP_left S1
LOG_LMFP_left_S1_SEP_Early=log(sum(GrandAVGSEPleftS1(1,1020:1323)));
LOG_LMFP_left_S1_SEP_Median=log(sum(GrandAVGSEPleftS1(1,1324:1639)));
LOG_LMFP_left_S1_SEP_Late=log(sum(GrandAVGSEPleftS1(1,1640:1943)));

% log value of TMS_LMFP_right S1
LOG_LMFP_right_S1_TMS_Early=log(sum(GrandAVGTMSrightS1(1,1020:1323)));
LOG_LMFP_right_S1_TMS_Median=log(sum(GrandAVGTMSrightS1(1,1324:1639)));
LOG_LMFP_right_S1_TMS_Late=log(sum(GrandAVGTMSrightS1(1,1640:1943)));
% log value of PAS_LMFP_right S1
LOG_LMFP_right_S1_PAS_Early=log(sum(GrandAVGPASrightS1(1,1020:1323)));
LOG_LMFP_right_S1_PAS_Median=log(sum(GrandAVGPASrightS1(1,1324:1639)));
LOG_LMFP_right_S1_PAS_Late=log(sum(GrandAVGPASrightS1(1,1640:1943)));
% log value of SEP_LMFP_right S1
LOG_LMFP_right_S1_SEP_Early=log(sum(GrandAVGSEPrightS1(1,1020:1323)));
LOG_LMFP_right_S1_SEP_Median=log(sum(GrandAVGSEPrightS1(1,1324:1639)));
LOG_LMFP_right_S1_SEP_Late=log(sum(GrandAVGSEPrightS1(1,1640:1943)));

%% bar plot of LMFP in 4 regions
figure;
LMFPLOG1=[LOG_LMFP_left_M1_SEP_Early,LOG_LMFP_left_M1_SEP_Median,LOG_LMFP_left_M1_SEP_Late];
LMFPLOG2=[LOG_LMFP_left_M1_TMS_Early,LOG_LMFP_left_M1_TMS_Median,LOG_LMFP_left_M1_TMS_Late];
LMFPLOG3=[LOG_LMFP_left_M1_PAS_Early,LOG_LMFP_left_M1_PAS_Median,LOG_LMFP_left_M1_PAS_Late];
LMFPLOGlm1(1,:)=LMFPLOG1;
LMFPLOGlm1(2,:)=LMFPLOG2;
LMFPLOGlm1(3,:)=LMFPLOG3;
bar(LMFPLOGlm1,'hist')
legend('Early','Median','Late')
ylabel('Log-LMFP(uV)')
title('Left M1')
set(gca,'XTickLabel','E-Stim|TMS|PAS')
ylim([0,8])

figure;
LMFPLOG1=[LOG_LMFP_right_M1_SEP_Early,LOG_LMFP_right_M1_SEP_Median,LOG_LMFP_right_M1_SEP_Late];
LMFPLOG2=[LOG_LMFP_right_M1_TMS_Early,LOG_LMFP_right_M1_TMS_Median,LOG_LMFP_right_M1_TMS_Late];
LMFPLOG3=[LOG_LMFP_right_M1_PAS_Early,LOG_LMFP_right_M1_PAS_Median,LOG_LMFP_right_M1_PAS_Late];
LMFPLOGrm1(1,:)=LMFPLOG1;
LMFPLOGrm1(2,:)=LMFPLOG2;
LMFPLOGrm1(3,:)=LMFPLOG3;
bar(LMFPLOGrm1,'hist')
legend('Early','Median','Late')
ylabel('Log-LMFP(uV)')
title('Right M1')
set(gca,'XTickLabel','E-Stim|TMS|PAS')
ylim([0 8])

figure;
LMFPLOG1=[LOG_LMFP_left_S1_SEP_Early,LOG_LMFP_left_S1_SEP_Median,LOG_LMFP_left_S1_SEP_Late];
LMFPLOG2=[LOG_LMFP_left_S1_TMS_Early,LOG_LMFP_left_S1_TMS_Median,LOG_LMFP_left_S1_TMS_Late];
LMFPLOG3=[LOG_LMFP_left_S1_PAS_Early,LOG_LMFP_left_S1_PAS_Median,LOG_LMFP_left_S1_PAS_Late];
LMFPLOGls1(1,:)=LMFPLOG1;
LMFPLOGls1(2,:)=LMFPLOG2;
LMFPLOGls1(3,:)=LMFPLOG3;
bar(LMFPLOGls1,'hist')
legend('Early','Median','Late')
ylabel('Log-LMFP(uV)')
title('Left S1')
set(gca,'XTickLabel','E-Stim|TMS|PAS')
ylim([0 8])

figure;
LMFPLOG1=[LOG_LMFP_right_S1_SEP_Early,LOG_LMFP_right_S1_SEP_Median,LOG_LMFP_right_S1_SEP_Late];
LMFPLOG2=[LOG_LMFP_right_S1_TMS_Early,LOG_LMFP_right_S1_TMS_Median,LOG_LMFP_right_S1_TMS_Late];
LMFPLOG3=[LOG_LMFP_right_S1_PAS_Early,LOG_LMFP_right_S1_PAS_Median,LOG_LMFP_right_S1_PAS_Late];
LMFPLOGrs1(1,:)=LMFPLOG1;
LMFPLOGrs1(2,:)=LMFPLOG2;
LMFPLOGrs1(3,:)=LMFPLOG3;
bar(LMFPLOGrs1,'hist')
legend('Early','Median','Late')
ylabel('Log-LMFP(uV)')
title('Right S1')
set(gca,'XTickLabel','E-Stim|TMS|PAS')
ylim([0 8])



