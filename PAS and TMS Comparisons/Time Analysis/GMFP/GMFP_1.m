%% TEP GMFP
%%%%%%%%%%%%the right code
M=mean(pe);
for i=1:size(pe,1)
    pf(i,:)=pe(i,:)-M(1,:);
end
pf=sqrt(sum(pf.^2)/62);

%% The wrong code
% for i=1:size(z,1) %p is the TEP data, ith is the channel's number
%     for j=1:size(z,2)    
%         m(:,j)= mean(z(:,j));
%         h(j)= sum((z(i,j)-m(1,j)).^2);
%         h(j)= h(j)/62;
%         h(j)= sqrt(h(j));% n is the GMFP
%     end   
% end


%% Grand average of GMFP by averaging these 4 subjects
GrandAVGSEP=(h+b+pf+n)/4;%SEP GMFP
GrandAVGTMS=(a+d+pd+l)/4;%TMS GMFP
GrandAVGPAS=(g+c+pc+m)/4;%PAS GMFP

figure
plot(data_tms_clean_avg.time,GrandAVGSEP)
hold on
plot(data_tms_clean_avg.time,GrandAVGTMS,'r')
plot(data_tms_clean_avg.time,GrandAVGPAS,'g')
grid on
legend('E-Stim','TMS','PAS')
xlabel('time (s)');
ylabel('Amp (uv)');
title('Grand average of GMFP')
xlim([-0.01 0.45])

%% Bar histograms represent integrated GMFP(log-GMFP)
%%%%% Early period is 0-150ms; Median period is 150-300ms; 
%%%%% Late period is 300-450ms. 
%%%%% The time window: 0-450ms based on my previous results.
% log value of SEP_GMFP
LOG_GMFP_SEP_Early=log(sum(GrandAVGSEP(1,1020:1323)));
LOG_GMFP_SEP_Median=log(sum(GrandAVGSEP(1,1324:1639)));
LOG_GMFP_SEP_Late=log(sum(GrandAVGSEP(1,1640:1943)));
LOG1=[LOG_GMFP_SEP_Early,LOG_GMFP_SEP_Median,LOG_GMFP_SEP_Late];
% log value of TMS_GMFP
LOG_GMFP_TMS_Early=log(sum(GrandAVGTMS(1,1020:1323)));
LOG_GMFP_TMS_Median=log(sum(GrandAVGTMS(1,1324:1639)));
LOG_GMFP_TMS_Late=log(sum(GrandAVGTMS(1,1640:1943)));
LOG2=[LOG_GMFP_TMS_Early,LOG_GMFP_TMS_Median,LOG_GMFP_TMS_Late];
% log value of PAS_GMFP
LOG_GMFP_PAS_Early=log(sum(GrandAVGPAS(1,1020:1323)));
LOG_GMFP_PAS_Median=log(sum(GrandAVGPAS(1,1324:1639)));
LOG_GMFP_PAS_Late=log(sum(GrandAVGPAS(1,1640:1943)));
LOG3=[LOG_GMFP_PAS_Early,LOG_GMFP_PAS_Median,LOG_GMFP_PAS_Late];
LOG(1,:)=LOG1;
LOG(2,:)=LOG2;
LOG(3,:)=LOG3;
figure;
bar(LOG,'grouped')
ylabel('Log-GMFP(uV)')
title('Log-GMFP VS E-Stim TMS & PAS')
set(gca,'XTickLabel','E-Stim|TMS|PAS')













