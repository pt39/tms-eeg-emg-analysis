% %% SEP GMFP
% eeglab % open eeglab and load data
% EEG = pop_loadeep('C:\Users\Administrator\Desktop\data\PAT_SEP.set' , 'triggerfile', 'on');
% EEG = pop_rmbase(EEG, []);%temove the baseline
% 
% [B,A]  = butter(4, [0.5 80]/(EEG.srate/2)); %butter filter
% EEGfiltered = filtfilt(B,A, double(EEG.data'))';
% 
% [B,A]  = butter(4, [55 65]/(EEG.srate/2), 'stop');%notch filter
% EEGfiltered = filtfilt(B,A, EEGfiltered')';
% 
% EEG.data = EEGfiltered;
% EEG = preExtraction(EEG);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % function EEG = preExtraction(EEG)
% %     for i=2:size(EEG.event,2)
% %         EEG.event(i).type=str2num(EEG.event(i).type);
% % 
% %         EEG.event(i).urevent=i;
% %     end
% %     EEG.event(1).type='EMPTY'; 
% % end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ALLEEG EEG] = eeg_store(ALLEEG, EEG);
% % extract epochs
% EEG = pop_epoch( EEG, {  '2'  }, [-0.5 1.5], 'newname', 'EEProbe continuous data epochs', 'epochinfo', 'yes');
% % calculate the mean of data
% z = mean(EEG.data, 3);
% % csalculate the GMFP
M=mean(z);
for i=1:size(z,1)
    h(i,:)=z(i,:)-M(1,:);
end
h=sqrt(sum(h.^2)/62);

