function dchani = getElectrodeNumber(EEG, dchan)
    for i = 1:length(EEG.chanlocs)
        if( strcmp(EEG.chanlocs(i).labels, dchan))
            dchani = i;
            return;
        end
    end    
        dchani = -1;
end