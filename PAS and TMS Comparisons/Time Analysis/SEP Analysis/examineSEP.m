%Example Usage: ALLEEG = examineSEP('YW',[]);eeglab redraw;
%the varargin input is for rejecting bad channels, default channels that
%are removed are M1, M2, VEOG, and HEOG
function ALLEEG = examineSEP(subjectname, refstr, varargin)
    paths;
    
    ALLEEG = eeglab; 
    
    bp_cutoff = [0.5 500];
    bs_cutoff = [55 65];
    
    if(~strcmp(subjectname, 'PAT'))
        eegfolder = cat(2, DATAPATH, '\', subjectname, '\', 'EEG\');
        filepath = [eegfolder 'SEP.cnt'];

        EEG = pop_loadeep(filepath , 'triggerfile', 'on');
        EEG = preExtraction(EEG);
        EEG = pop_reref(EEG, []);
        EEG = pop_select(EEG, 'nochannel', {'VEOG', 'HEOG'});

        if(nargin>2)
            EEG = pop_select(EEG, 'nochannel', varargin{1});
        end
        EEG = pop_chanedit(EEG, 'lookup',[EEGLABPATH '\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp']);

        [B_bp,A_bp] = butter(2, bp_cutoff/(EEG.srate/2));
        [B_bs,A_bs] = butter(4, bs_cutoff/(EEG.srate/2), 'stop');
        EEG.data = filtfilt(B_bp, A_bp, double(EEG.data'))';
        EEG.data = filtfilt(B_bs, A_bs, double(EEG.data'))';
    else

        eegfolder = cat(2, DATAPATH, '\', subjectname, '\', 'EEG\');
        filepath = [eegfolder '20140227_1535.cnt'];
        EEG1 = pop_loadeep(filepath , 'triggerfile', 'on');
        
        [B_bp,A_bp] = butter(2, bp_cutoff/(EEG1.srate/2));
        [B_bs,A_bs] = butter(4, bs_cutoff/(EEG1.srate/2), 'stop');
        
        EEG1 = preExtraction(EEG1);
        EEG1 = pop_reref(EEG1, []);
        EEG1 = pop_select(EEG1, 'nochannel', {'VEOG', 'HEOG'});
    
        if(nargin>2)
            EEG1 = pop_select(EEG1, 'nochannel', varargin{1});
        end
        EEG1 = pop_chanedit(EEG1, 'lookup',[EEGLABPATH '\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp']);

        EEG1.data = filtfilt(B_bp, A_bp, double(EEG1.data'))';
        EEG1.data = filtfilt(B_bs, A_bs, double(EEG1.data'))';
        
        %second eeg dataset
        filepath = [eegfolder '20140227_1553.cnt'];
        EEG2 = pop_loadeep(filepath , 'triggerfile', 'on');
        
        EEG2 = preExtraction(EEG2);
        EEG2 = pop_reref(EEG2, []);
        EEG2 = pop_select(EEG2, 'nochannel', {'VEOG', 'HEOG'});

        if(nargin>2)
            EEG2 = pop_select(EEG2, 'nochannel', varargin{1});
        end
        EEG2 = pop_chanedit(EEG2, 'lookup',[EEGLABPATH '\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp']);

        EEG2.data = filtfilt(B_bp, A_bp, double(EEG2.data'))';
        EEG2.data = filtfilt(B_bs, A_bs, double(EEG2.data'))';
        
        EEG = pop_mergeset(EEG1, EEG2);
    end
    EEG.setname = 'filtered';
    
    EEG = pop_epoch(EEG, {'1'}, [-.01 .25]);
    EEG = pop_rmbase( EEG, [-10 0]);
    
    if(isempty(refstr))
        EEG = pop_reref(EEG, []);
    else
        EEG = pop_reref(EEG, getElectrodeNumber(EEG,refstr));
    end
    
     EEG = pop_select(EEG, 'nochannel', {'M1', 'M2'});
    ALLEEG = eeg_store(ALLEEG, EEG);
    
    figure; pop_timtopo(EEG, [-10 100], [20 27 45], subjectname, 'maplimits', [-3 3]);
    
    allAxes = findall(gcf,'type','axes');
    axes(allAxes(end-1));
    ylim([-4 4]);
    axes(allAxes(1));
    
    %write information for LORETA CSD estimation in specific subject folder
    if(exist(subjectname, 'dir') ~=7)
        mkdir(subjectname);
    end
    
    %file for channel locations
    fid = fopen([subjectname '/chanlocs.txt'], 'w');

    for i=1:length(EEG.chanlocs)
        fprintf(fid, '%s\n', EEG.chanlocs(i).labels);
    end
    fclose(fid);

    erp = double(mean(EEG.data,3))';

    %file for erp data
    fid = fopen([subjectname '/erp.asc'], 'w');

    for i = 1:size(erp,1)
        for j = 1:size(erp,2)
            if(erp(i,j) < 0)
                fprintf(fid,' %4.20e', erp(i,j)); 
            else
                fprintf(fid,'  %4.20e', erp(i,j)); 
            end
        end 
        fprintf(fid,'\n');
    end
    fclose(fid);

end