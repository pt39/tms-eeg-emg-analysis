function plotSEP(currcond, msg)
    ylimvals = [-10 10];
    cfg = [];
    
    cfg.layout = 'EEG1010_prasad.lay';
    cfg.ylim = [-10 20];
    cfg.showlabels = 'yes';
    
    figure;
    cfg.comment = msg;
    subplot(3,2,1:2:5);
    ft_multiplotER(cfg, currcond);
    
    dchan.c3 = find(strcmp(currcond.label, 'C3'));
    dchan.p3 = find(strcmp(currcond.label, 'P3'));
    dchan.cp3 = find(strcmp(currcond.label, 'CP3'));
    
    subplot(3,2,2);
    plot(currcond.time*1000, zeros(size(currcond.time)), 'k--');hold on;
    plot(currcond.time*1000, currcond.avg(dchan.c3,:));
    xlim([currcond.time(1) currcond.time(end)]*1000);ylim(ylimvals);set(gca,'xticklabel',{[]}) 
    ylabel('C3')
    subplot(3,2,4);
    plot(currcond.time*1000, zeros(size(currcond.time)), 'k--');hold on;
    plot(currcond.time*1000, currcond.avg(dchan.p3,:))
    xlim([currcond.time(1) currcond.time(end)]*1000);ylim(ylimvals);set(gca,'xticklabel',{[]}) 
    ylabel('P3');
    subplot(3,2,6);
    plot(currcond.time*1000, zeros(size(currcond.time)), 'k--');hold on;
    plot(currcond.time*1000, currcond.avg(dchan.cp3,:))
    xlim([currcond.time(1) currcond.time(end)]*1000);ylim(ylimvals);
    ylabel('CP3');
    xlabel('Time(ms)');
end