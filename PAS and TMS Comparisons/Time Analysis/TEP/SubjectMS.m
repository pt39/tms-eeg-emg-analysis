paths;

%FieldTrip ERPs
tms = load([FTDATAPATH, '\MStmserp.mat']);
tms = tms.data_tms_clean_avg;
pas =load([FTDATAPATH, '\MSpaserp.mat']);
pas = pas.data_tms_clean_avg;

%14 is Cz
figure;
plot(pas.time,pas.avg(15, :),tms.time, tms.avg(15,:));
xlim([-0.03 1.2]);


set(gca, 'fontsize', 30);
title('Subject MS TEP Comparison Channel Cz');
legend('PAS', 'TMS Only');
xlabel('Time(s)');
ylabel('Amplitude(\muV)');