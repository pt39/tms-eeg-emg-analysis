function EEG = preExtraction(EEG)
    for i=2:size(EEG.event,2)
        EEG.event(i).type=str2num(EEG.event(i).type);

        EEG.event(i).urevent=i;
    end
    EEG.event(1).type='EMPTY'; 
end