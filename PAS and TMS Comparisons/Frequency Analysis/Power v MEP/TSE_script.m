eeglab 
subject_list = ls(['R:\TMS-EEG\raw data\']);
pathname1='R:\TMS-EEG\raw data\';
    totalCount=1;
for s=4:5
    pathname = [pathname1  strtrim(subject_list(s,:))];
    pathname2=[pathname, '\', 'EEG\PAS.cnt'];

%     study(s).SubjectName=[];
    study(s).SubjectName=strtrim(subject_list(s,:));

    EEG = pop_loadeep(pathname2 , 'triggerfile', 'on');
    EEG = preExtraction(EEG);
    EEG = pop_select( EEG,'nochannel',{'HEOG', 'VEOG'});
    EEG = pop_reref( EEG, []);
    EEG.setname='SVA PAS';
    EEG = pop_rmbase( EEG, []);
    EEG = pop_epoch( EEG, {  '2'  }, [-1      -0.005]);
    %%
    EEGraw = EEG;

    % cluster into 4 groups: left rolandic, right rolandic, frontal, occipital

    EEGlr = pop_select( EEGraw,'channel',{'C1', 'CP1','C3', 'CP3'});
    EEGrr = pop_select( EEGraw,'channel',{'C2', 'CP2','C4', 'CP4'});
    EEGfr = pop_select( EEGraw,'channel',{'Fp1', 'Fpz','Fp2', 'AF3', 'AF4'});
    EEGocc = pop_select( EEGraw,'channel',{'P1', 'Pz','P2', 'PO4', 'POz', 'PO3'});

    clusters = {EEGlr, EEGrr, EEGfr, EEGocc};
    clusternames = {'LeftRolandic', 'RightRolandic', 'Frontal', 'Occipital'};
    

    %get the meps
    [PAS_MEPamp, TMS_MEPamp] = EMGAnalysis('SVA', 1,false);
    relevantMEP = PAS_MEPamp; % <- change for tms

    relevantMEPWithRejection = relevantMEP(~isnan(relevantMEP));

    [orderedMEPs, I] = sort(relevantMEPWithRejection);

    onethird = round(length(I)/3);
    twothird = 2*round(length(I)/3);
    mepClusters = {I(1:onethird) , ...
                    I(onethird+1:twothird), ...
                    I(twothird+1:end)};
    mepClusterNames = {'small', 'medium', 'large'};
    %temporal spectral evolution: first filter each cluster, then rectify the
    %filtered signal 

    %%
    figure
    alpha=[];
    lowbeta=[];
    midbeta=[];
    highbeta=[];
    frequencyranges={alpha,lowbeta,midbeta,highbeta};
    frequencyrangesNames={'alpha','lowbeta','midbeta','highbeta'};
    frequencyrange.band={[8 12.5], [12 15], [15 18], [18 30]};
    for k=1:length(frequencyranges)

    [B,A]  = butter(2, cell2mat(frequencyrange.band(k))/EEG.srate/2); %alpha

        for i = 1:length(clusters)
            EEGcurr = clusters{i};
            EEGcurr = pop_rejepoch(EEGcurr, isnan(relevantMEP),0);
                for j = 1:size(EEGcurr.data,3)   
                    EEGcurr.data(:,:,j) = filtfilt(B,A, double(EEGcurr.data(:,:,j)'))';
                    EEGcurr.data = abs(EEGcurr.data);
                end
            clusters{i} = EEGcurr;
        end

    %group EEG clusters w.r.t the mep amplitude



%     study(s).(frequencyrangesNames(k)).name = frequencyrangesNames(k);

    for  i = 1:length(clusters)
            EEGcurr = clusters{i};% current tse waveform for particular cluster
        for j = 1:2:length(mepClusters)
            EEGmepsubset = pop_select(EEGcurr, 'trial', mepClusters{j});
            EEGchannelaverage = squeeze(mean(EEGmepsubset.data));
            EEGtrialaverage = mean(EEGchannelaverage, 2);
            hold on
            subplot(4,4,i+4*(k-1));
            plot(EEGcurr.times, EEGtrialaverage);
            title(clusternames{i})
            ylabel(frequencyrangesNames(k))

            prestiminterval = find(EEGcurr.times>=-293 & EEGcurr.times <=-5);
            hold on
            plot(EEGcurr.times(prestiminterval),EEGtrialaverage(prestiminterval),'k', 'linewidth',2);

            %study.frequencyRanges(k).(clusternames{i}).(mepClusterNames{j}) = EEGtrialaverage(prestiminterval);
            study(s).(frequencyrangesNames{k}).(clusternames{i}).(mepClusterNames{j}) = EEGtrialaverage(prestiminterval);
            
        end

    end


    end
end
%%
% get grand average tse waveforms

study(end+1).SubjectName = 'grandave';

for i = 4:14 %iterate over studies
    
    for j = 1:length(frequencyrangesNames) %iterates over frequencies
        f = fields(study(i).(frequencyrangesNames{j}));
        for k = 1:length(f)% iterates channel clusters
            subf = fields(study(i).(frequencyrangesNames{j}).(f{k}));
            for l = 1:length(subf) %iterates over mep clusters
                
                currtse = study(i).(frequencyrangesNames{j}).(f{k}).(subf{l});
                
                str= sprintf('study(end).%s.%s.%s', frequencyrangesNames{j}, f{k}, subf{l});
                try
                    study(end).(frequencyrangesNames{j}).(f{k}).(subf{l})(end+1,:) = currtse';
                catch error
                    study(end).(frequencyrangesNames{j}).(f{k}).(subf{l}) = currtse';
                    
                end
            end
        end
        
    end    
end
    
    