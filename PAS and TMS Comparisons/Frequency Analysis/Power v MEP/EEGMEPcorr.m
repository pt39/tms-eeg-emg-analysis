function EEGMEPcorr(condition,TimeWindow,DatasetName,MEPfile,MuPower,TMS_MEPamp)
% example : EEGMEPcorr(PowSpreadsheetName,DatasetName,'SVA_MEPanalysis.mat')


    for i=1:6
%         clear PASMEPvalues
%         clear eegpowersI
        switch i
            case 1
                xlabelstr='EEGpower in alpha band';
            case 2
                xlabelstr='EEGpower in beta band';
            case 3
                xlabelstr='EEGpower in beta1 band';  
            case 4
                xlabelstr='EEGpower in beta2 band';
            case 5
                xlabelstr='EEGpower in beta3 band'; 
            case 6
                xlabelstr='EEGpower in gamma band';
        end


  eegpowers=MuPower;
  eegpowersI=eegpowers(:,i);
%%%%%%%%%%%%%%%%%%%%%%%%%
% MEPs=load(MEPfile);    
%     if strcmpi(condition,'PAS')
%     PASMEPvalues=MEPs.PAS_MEPamp;
%     else
%         PASMEPvalues=MEPs.TMS_MEPamp;
%     end
    

%%%%%%%%%%%
      
        Poweroutliers=find(eegpowersI>4*std(eegpowersI));
        eegpowersI(Poweroutliers)=NaN;
        
          
    if strcmpi(condition,'PAS')
    PASMEPvalues=PAS_MEPamp;
    else
        PASMEPvalues=TMS_MEPamp;
    end
        %PASMEPvalues=PAS_MEPamp;
        PASMEPvalues(Poweroutliers)=NaN;

%%%%%%%

eegpowersI(find(isnan(PASMEPvalues)))=NaN; 
PASMEPvalues(find(isnan(eegpowersI)))=NaN;
    

    
PASMEPvalues =  PASMEPvalues(~isnan(PASMEPvalues));
eegpowersI=eegpowersI(~isnan(eegpowersI));
%%
    h(i)=figure
    plot(eegpowersI,PASMEPvalues,'*')
    P = polyfit(eegpowersI,PASMEPvalues,1)
   % yfit = P(1)*(eegpowersI)+P(2);
        hold on;
       % plot(eegpowersI,yfit,'m','LineWidth',3);
         
         display(P(1))
         li=polyval(P,eegpowersI)
         hold on
         plot(eegpowersI,li,'m','LineWidth',3)
      [b,bint,r,rint,stats] = regress(eegpowersI,PASMEPvalues);
         display(stats)   
         text(double(min(eegpowersI)),double(max(PASMEPvalues)),['y = ',mat2str(P(1),2),'x + ',mat2str(P(2),2)],'FontWeight','BOLD');
         %text(max(eegpowersI),max(PASMEPvalues) , ['Rsq=',num2str(stats(1))],'FontWeight','BOLD');

         % text(min(eegpowersI),max(PASMEPvalues) , ['Rsq=',num2str(stats(1))],'FontWeight','BOLD');
    
title(cat(1,[],{DatasetName, TimeWindow}))
    xlabel(xlabelstr)
    ylabel('MEP Amp (mV)')
%%    
    figure
    X = 1:1:size(MuPower);
    plot(X,eegpowersI,'*','r')
    title(xlabelstr)
    xlabel('trial number')
    ylabel('power')
    
saveppt2('EEGpowerCorr2.ppt')
st=[];
 st=cat(1,st,{DatasetName xlabelstr TimeWindow num2str(mat2str(P(1),2)) });
 %xlswrite('Rsq Table.xls',st,'Rsq');
 xlsappend('Rsq Table.xls',st,'Rsq');
end
