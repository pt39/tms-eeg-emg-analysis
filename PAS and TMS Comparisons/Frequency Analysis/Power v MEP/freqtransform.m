function [MuPower2, Pyydata, freq] =testfreqtransform(data,fs,ChanNum)
%[MuPower, Pyydata, freq] =testfreqtransform(EEG.data,EEG.srate,15)
DataSize = size(data);

NumTrials = DataSize(3);

%Grab Channel

for TrialNum = 1:size(data,3)
assignin('base','TrialNum',TrialNum);
ChanData(TrialNum,:) = data(ChanNum,:,TrialNum);

[Pyydata(TrialNum,:), freq(TrialNum,:)] = PSDmyNew(ChanData(TrialNum,:)',fs);
[Pyydata2(TrialNum,:), freq2(TrialNum,:)] = spectopo(single(data(ChanNum,:,TrialNum)), 0, fs,'plot', 'off' );    

Pyydata2(TrialNum,:) = db2pow(Pyydata2(TrialNum,:));
%%
% plot(freq(TrialNum,:),Pyydata(TrialNum,:),freq2(TrialNum,:),Pyydata2(TrialNum,:));
% legend('fft', 'eeglab-welch');
% xlim([0 50]);
%%
%look at beta range
%MuInds = find(freq(TrialNum,:)>=8 & freq(TrialNum,:)<=13);

MuPower(TrialNum,:) = trapz(freq(TrialNum,:),Pyydata(TrialNum,:));
MuPower2(TrialNum,:) = trapz(freq2(TrialNum,:),Pyydata2(TrialNum,:));

% p(TrialNum) = bandpower(ChanData(TrialNum,:),fs,[8 13]);

% plot(freq(TrialNum,1:end/2),Pyydata(TrialNum,1:end/2))
% 
% title(['TrialNum ', mat2str(TrialNum)])

%xlim([0 40])

 %pause(1)

% cla

end
% save MuPower
% save  Pyydata
% save freq
figure

function [Pyydata,freq] = PSDmyNew(data,fs)

L = length(data);

NFFT = 2^nextpow2(L); % Next power of 2 from length of y

index = 1:NFFT;

fres = fs/NFFT;

freq = index.*fres;

WindowType = hamming(NFFT)';

Ydata = fft(data,NFFT)/L;

Pyydata = Ydata.* conj(Ydata);

Pyydata(1) = 0;
