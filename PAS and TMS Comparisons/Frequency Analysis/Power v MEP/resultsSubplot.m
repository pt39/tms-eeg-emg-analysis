clear
close all

    for i=1:6
   %%
        switch i
        case 1
            xlabelstr='EEGpower in alpha band';
            
        case 2
            xlabelstr='EEGpower in beta band';
            
        case 3
            xlabelstr='EEGpower in beta1 band';
            
        case 4
            xlabelstr='EEGpower in beta2 band';
           
        case 5 
            xlabelstr='EEGpower in beta3 band';
            
        case 6
            xlabelstr='EEGpower in gamma band'; 
    end
%%    
eegpowers=xlsread('powers.xlsx','JSD-refAVG');
TMSMEPs=load('JSD_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>3*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;

eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));
figure
subplot(4,5,1:3), plot(eegpowersInorm,'*')
hold on 
title('JSD-refAVG')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
%legend(xlabelstr,'MEPamp')
axisLimitforPlot=max(max(eegpowersI),max(PASMEPvalues));
subplot(4,5,4:5),
% axis([0 axisLimitforPlot 0 axisLimitforPlot])
%  hold on    
plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
%%%%%%%%%%%
%%
eegpowers=xlsread('powers.xlsx','MS-refAVG');
TMSMEPs=load('MS_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>3*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,6:8),plot(eegpowersInorm,'*')
hold on 
title('MS-refAVG')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,9:10),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')

%%
eegpowers=xlsread('powers.xlsx','SVA-refAVG');
TMSMEPs=load('SVA_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>10*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,11:13)
plot(eegpowersInorm,'*')
hold on 
title('SVA-refAVG')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,14:15),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')

subplot(4,5,16:18)
subplot(4,5,19:20)
%%
eegpowers=xlsread('powers.xlsx','PAT-refAVG');
TMSMEPs=load('PAT_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>3*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,6:8),plot(eegpowersInorm,'*')
hold on 
title('PAT-refAVG')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,9:10),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
%%
clear PASMEPvalues
clear eegpowersI
figure
eegpowers=xlsread('powers.xlsx','JSD-refC3C4');
TMSMEPs=load('JSD_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,1:3), plot(eegpowersInorm,'*')
hold on 
title('JSD-refC3C4')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
axisLimitforPlot=max(max(eegpowersI),max(PASMEPvalues));
subplot(4,5,4:5),
% axis([0 axisLimitforPlot 0 axisLimitforPlot])
%  hold on    
plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
%%%%%%%%%%%
%%
eegpowers=xlsread('powers.xlsx','MS-refC3C4');
TMSMEPs=load('MS_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>3*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,6:8),plot(eegpowersInorm,'*')
hold on 
title('MS-refC3C4')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,9:10),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
%%

eegpowers=xlsread('powers.xlsx','SVA-refC3C4');
TMSMEPs=load('SVA_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>8*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,11:13)
plot(eegpowersInorm,'*')
hold on 
title('MS-refC3C4')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,14:15),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
subplot(4,5,16:18)
subplot(4,5,19:20)
%%
eegpowers=xlsread('powers.xlsx','PAT-refC3C4');
TMSMEPs=load('PAT_MEPanalysis.mat');    
PASMEPvalues=TMSMEPs.PAS_MEPamp;
eegpowersI=eegpowers(:,i);
Poweroutliers=find(eegpowersI>3*std(eegpowersI));
eegpowersI(Poweroutliers)=NaN;
PASMEPvalues(Poweroutliers)=NaN;
eegpowersInorm = (eegpowersI - min(eegpowersI))/(max(eegpowersI) - min(eegpowersI));
PASMEPvaluesnorm = (PASMEPvalues - min(PASMEPvalues))/(max(PASMEPvalues) - min(PASMEPvalues));

subplot(4,5,6:8),plot(eegpowersInorm,'*')
hold on 
title('PAT-refC3C4')
%legend('EEGpower in alpha band')
plot(PASMEPvaluesnorm,'r*')
legend(xlabelstr,'MEPamp')
subplot(4,5,9:10),plot(eegpowersI,PASMEPvalues,'*')
xlabel(xlabelstr)
ylabel('PAS MEPamp')
%%
saveppt2('EEGpower-MEP subplots.ppt')

%%

    end


