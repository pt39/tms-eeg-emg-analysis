%Example usage: [PAS_MEPamp, TMS_MEPamp] = EMGAnalysis('NBW', 1,true) <-true if you want to exclude
%meps with amplitudes less than 0.05
function [PAS_MEPamp, TMS_MEPamp] = EMGAnalysis(SubjectName, NumSess, rejectBelowMotorTreshold)
    paths;

    fs = 2048;
    MEPplotWin = round([ .05*fs .1*fs ]);
    MEPplotInds = ((-1*MEPplotWin(1)) : MEPplotWin(2)) ./fs.*1000; % in ms 
    MEPfindWin = round([ .01*fs .04*fs ]);
    MEPfindInds = MEPplotWin(1) + MEPfindWin;

    SEP_EMGbyStim = [];
    TMS_EMGbyStim = [];
    PAS_EMGbyStim = [];

    for SessNum = 1:NumSess

        load([DATAPATH,'\',SubjectName,'\EMG Files\SEP',mat2str(SessNum)]);
        SEP_EMGbyStimTmp = parseEMG(EMG,EventFlags,MEPplotWin,fs);
        SEP_EMGbyStim = [SEP_EMGbyStim;SEP_EMGbyStimTmp];
        clear('EMG','EventFlags','Data')

        load([DATAPATH,'\',SubjectName,'\EMG Files\TMS',mat2str(SessNum)]);
        TMS_EMGbyStimTmp = parseEMG(EMG,EventFlags,MEPplotWin,fs);
        TMS_EMGbyStim = [TMS_EMGbyStim;TMS_EMGbyStimTmp];
        clear('EMG','EventFlags','Data')

        load([DATAPATH,'\',SubjectName,'\EMG Files\PAS',mat2str(SessNum)]);
        PAS_EMGbyStimTmp = parseEMG(EMG,EventFlags,MEPplotWin,fs);
        PAS_EMGbyStim = [PAS_EMGbyStim;PAS_EMGbyStimTmp];
        clear('EMG','EventFlags','Data')
    end

    if SubjectName(1:2) == 'MS'
        TMS_EMGbyStim(1:24,:) = NaN;
    end

    if SubjectName(1:2) == 'JS'
        PAS_EMGbyStim(302:end,:) = NaN;
    end
    
    PAS_EMGbyStimRaw = PAS_EMGbyStim;
    %Calculate MEP amplitude and organise data by trial
    MeanSEP = mean(SEP_EMGbyStim);
    DataSize = size(SEP_EMGbyStim);
    NumTrials = DataSize(1);

    PAS_EMGbyStimRaw = PAS_EMGbyStim;
    for TrialNum = 1:NumTrials        
        TMS_MaxAmp(TrialNum,:) = max(TMS_EMGbyStim(TrialNum,MEPfindInds(1):MEPfindInds(2)));
        TMS_MinAmp(TrialNum,:) = min(TMS_EMGbyStim(TrialNum,MEPfindInds(1):MEPfindInds(2)));
        TMS_MEPamp(TrialNum,:) = TMS_MaxAmp(TrialNum,:)-TMS_MinAmp(TrialNum,:);

        PAS_EMGbyStim(TrialNum,:) = PAS_EMGbyStim(TrialNum,:) - MeanSEP; %Note here in order to remove the estim artifact the mean SEP is subtracted from the PAS-MEP
        PAS_MaxAmp(TrialNum,:) = max(PAS_EMGbyStim(TrialNum,MEPfindInds(1):MEPfindInds(2)));
        PAS_MinAmp(TrialNum,:) = min(PAS_EMGbyStim(TrialNum,MEPfindInds(1):MEPfindInds(2)));
        PAS_MEPamp(TrialNum,:) = PAS_MaxAmp(TrialNum,:)-PAS_MinAmp(TrialNum,:);
    end
    
    %reject trials with mep amplitudes lower than motor threshold
    if(rejectBelowMotorTreshold)
        TMS_MEPamp(TMS_MEPamp<=0.05) = NaN;
        PAS_MEPamp(PAS_MEPamp<=0.05) = NaN;
    end
    
    figure         
    subplot(2,2,1)
    for numlines = 1:size(TMS_EMGbyStim,1)
        plot(MEPplotInds,TMS_EMGbyStim(numlines,:),'color', [numlines/size(TMS_EMGbyStim,1), 0, 1-numlines/size(TMS_EMGbyStim,1)])
        hold on;
    end
    hold on
    line([0,0],[-.5,.5],'Color','g','LineWidth',3)
    line([10,10],[-.5,.5],'Color','k','LineWidth',1)
    line([40,40],[-.5,.5],'Color','k','LineWidth',1)
    xlim([-50 100])
    ylim([-.5 .5])
    title('TMSonly')
    ylabel('Voltage (mV)')
    xlabel('Time (ms)')

    subplot(2,2,3)
    x = 1:NumTrials;
    x = x(~isnan(TMS_MEPamp));
    TMS_MEPampTmp = TMS_MEPamp(~isnan(TMS_MEPamp));
    plot(x,TMS_MEPampTmp,'o');
    
    P = polyfit(x,TMS_MEPampTmp',1);
    yfit = P(1)*(x)+P(2);
    hold on;
    plot(x,yfit,'k','LineWidth',3);
    text(10,max([TMS_MEPamp;PAS_MEPamp]),['y = ',mat2str(P(1),2),'x + ',mat2str(P(2),2)],'FontWeight','BOLD')
    xlim([0 400])
    ylim([0 max([TMS_MEPamp;PAS_MEPamp])+(.2*max([TMS_MEPamp;PAS_MEPamp]))])
    xlabel('Trial')
    ylabel('MEP Amp (mV)')

    subplot(2,2,2)
    for numlines = 1:size(PAS_EMGbyStim,1)
        plot(MEPplotInds,PAS_EMGbyStim(numlines,:),'color', [numlines/size(PAS_EMGbyStim,1), 0, 1-numlines/size(PAS_EMGbyStim,1)])
        hold on;
    end
    line([0,0],[-.5,.5],'Color','g','LineWidth',3)
    line([10,10],[-.5,.5],'Color','k','LineWidth',1)
    line([40,40],[-.5,.5],'Color','k','LineWidth',1)
    axis tight   
    xlim([-50 100])
    ylim([-.5 .5])
    title('PAS')
    ylabel('Voltage (mV)')
    xlabel('Time (ms)')

    subplot(2,2,4)
    x = 1:NumTrials;
    x = x(~isnan(PAS_MEPamp));
    PAS_MEPampTmp =  PAS_MEPamp(~isnan(PAS_MEPamp));
    plot(x,PAS_MEPampTmp,'o');
    
    P = polyfit(x,PAS_MEPampTmp',1);
    yfit = P(1)*(x)+P(2);
    hold on;
    plot(x,yfit,'k','LineWidth',3);
    text(10,max([TMS_MEPamp;PAS_MEPamp]),['y = ',mat2str(P(1),2),'x + ',mat2str(P(2),2)],'FontWeight','BOLD')
    xlim([0 400])
    ylim([0 max([TMS_MEPamp;PAS_MEPamp])+(.2*max([TMS_MEPamp;PAS_MEPamp]))])
    xlabel('Trial')
    ylabel('MEP Amp (mV)')   
    
    fprintf('Number of trials removed from TMS: %d\nNumber of trials removed from PAS: %d\n', sum(isnan(TMS_MEPamp)), sum(isnan(PAS_MEPamp)));
end