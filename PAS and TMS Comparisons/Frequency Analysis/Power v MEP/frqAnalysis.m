% this script is for frequency analysis and MEP/power correlations
function frqAnalysis(condition,minWindow,maxWindow,DatasetName,MEPfile,ChanNum,TimeWindow)
%Example:   frqAnalysis('TMS',.2,.4,'YW PAS','YW_MEP.mat',15,'[.2 .4]')
close all
    load('subejctyw_datatmsclean2_pas.mat') %this .mat file is fieldtrip output
    a=data_tms_clean2.trial;
    data=[];
    % ii=find(data_tms_clean2.time{1} >=- 0.1 & data_tms_clean2.time{1} <= -.005);%time window changes

    ii=find(data_tms_clean2.time{1} >=minWindow & data_tms_clean2.time{1} <= maxWindow);%time window changes

    for i=1:length(a)
    
        data(:,:,i)=a{i}(:,ii);
    end
    srate=2048;
    [freq] =plotFreq(data,srate,DatasetName) %for surf plotting freq vs.trial vs.power
    MuPower=FindPowerBands(data,srate,ChanNum,DatasetName,TimeWindow);
    EEGMEPcorr(condition,'[.2 .4]',DatasetName,MEPfile,MuPower);
end