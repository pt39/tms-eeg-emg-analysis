
function [freq] =plotFreq(data,srate,DatasetName)
%[freq] =plotFreq(EEG.data,EEG.srate,'DatasetName');
[MuPower, Pyydata, freq] =freqtransform(data,srate,15);
X = 1:size(data,3);
Y = freq;
Z = double(Pyydata);
%surf(X,Y(1,1:200),Z(:,1:200)', 'edgecolor', 'none')
surf(X,Y(1,1:200),Z(:,1:200)', 'edgecolor', 'none')
xlabel('Trials');
ylabel('Frequency');
zlabel('Power');
title(DatasetName);

view(0,90)
colorbar
%saveppt2('Frequency plots.ppt')
 end