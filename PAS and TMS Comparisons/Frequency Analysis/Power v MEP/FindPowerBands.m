function MuPower=FindPowerBands(data,fs,ChanNum,DatasetName,TimeWindow);
%MuPower=FindPowerBands(EEG.data,EEG.srate,15,'JSD-refAVG');
for i=1:6
    switch i
            case 1
                str='EEGpower in alpha band';
                minFreq=8;
                maxFreq=13;
            case 2
                str='EEGpower in beta band';
                minFreq=14;
                maxFreq=20;
            case 3
                str='EEGpower in beta1 band';
                    minFreq=12.5;
                    maxFreq=16;
            case 4
                str='EEGpower in beta2 band';
                    minFreq=16.5;
                    maxFreq=20;
            case 5 
                str='EEGpower in beta3 band';
                    minFreq=20.5;
                    maxFreq=28;
            case 6
                   str='EEGpower in gamma band'; 
                   minFreq=21;
                   maxFreq=50;
    end

    DataSize = size(data);
    NumTrials = DataSize(3);

    %Grab Channel

    for TrialNum = 1:size(data,3)
        assignin('base','TrialNum',TrialNum);
        ChanData(TrialNum,:) = data(ChanNum,:,TrialNum);

        [Pyydata(TrialNum,:),freq(TrialNum,:)] = PSDmyNew(ChanData(TrialNum,:)',fs);
        plot(freq(TrialNum,:),Pyydata(TrialNum,:))
        %look at beta range
        MuInds = find(freq(TrialNum,:)>=minFreq & freq(TrialNum,:)<=maxFreq);

        if(length(MuInds)>1)
            MuPower(TrialNum,i) = trapz(freq(TrialNum,MuInds),Pyydata(TrialNum,MuInds))/(freq(TrialNum,MuInds(end)) - freq(TrialNum,MuInds(1)));
        else
            MuPower(TrialNum,i) = Pyydata(TrialNum,MuInds);
        end
        % p(TrialNum) = bandpower(ChanData(TrialNum,:),fs,[8 13]);

        %plot(freq(TrialNum,1:end/2),Pyydata(TrialNum,1:end/2))

        %title(['TrialNum ', mat2str(TrialNum)])

        xlim([0 40])

        % pause(1)

        cla

    end


end
 st=[];
 st=cat(1,st,{ 'alphaPower' 'betaPower' 'beta1Power' 'beta2Power' 'beta3Power' 'gamaPower' });
 
%  xlswrite(PowSpreadsheetName,st,DatasetName);
%  xlsappend(PowSpreadsheetName,MuPower,DatasetName);
 
function [Pyydata,freq] = PSDmyNew(data,fs)

L = length(data);

NFFT = 2^nextpow2(L); % Next power of 2 from length of y

index = 1:NFFT;

fres = fs/NFFT;

freq = index.*fres;

WindowType = hamming(NFFT)';

Ydata = fft(data,NFFT)/L;

Pyydata = Ydata.* conj(Ydata);

Pyydata(1) = 0;
