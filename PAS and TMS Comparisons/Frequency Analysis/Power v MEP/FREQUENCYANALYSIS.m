%%
DatasetName='CM2 PAS';
MEPfile='CM2_MEP.mat';
NAME='CM2';
cd('C:\Users\Maryam\Documents\tms-eeg-emg-analysis\PAS and TMS Comparisons\MEP Analysis')
[PAS_MEPamp, TMS_MEPamp] = EMGAnalysis(NAME, 1,true)
%LOAD EXISTING DATA SET THROUGH EEGLAB
%check the trials that you have deleted 
%%
cd('C:\Users\Maryam\Documents\tms-eeg-emg-analysis\PAS and TMS Comparisons\Frequency Analysis\Power v MEP')
    [freq] =plotFreq(EEG.data,EEG.srate,DatasetName) %for surf plotting freq vs.trial vs.power
%%
    MuPower=FindPowerBands(EEG.data,EEG.srate,15,DatasetName,'[.5 3.5]');
    EEGMEPcorr('PAS','[.5 3.5]',DatasetName,MEPfile,MuPower,PAS_MEPamp);
    %%