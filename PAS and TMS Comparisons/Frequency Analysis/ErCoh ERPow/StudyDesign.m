matfiles = ls('*mat');
matfiles = matfiles(2:2:end,:);
nSubjects = size(matfiles, 1);

powyaxis = [-50 150];
cohyaxis = [-.3 .25];
for i = 1:nSubjects
    fnPAS = matfiles(i,matfiles(i,:)~=' ');
    fnTMS = cat(2,fnPAS(1:end-4), '-TMS.mat');
    subjectName = fnPAS(8:end-4);
    
    subjectPAS = load(fnPAS);
    subjectTMS = load(fnTMS);
    
    study.subject(i).name = subjectName;
    study.subject(i).pas.paper = subjectPAS.paper;
    study.subject(i).tms.paper = subjectTMS.paper;
    
    study.pas.ERPow.epoch1.alpha(i,:) = subjectPAS.paper.ERPow.epoch1.alpha;
    study.pas.ERPow.epoch2.alpha(i,:) = subjectPAS.paper.ERPow.epoch2.alpha;
    
    study.pas.ERPow.epoch1.beta(i,:) = subjectPAS.paper.ERPow.epoch1.beta;
    study.pas.ERPow.epoch2.beta(i,:) = subjectPAS.paper.ERPow.epoch2.beta;
    
    study.pas.ERCoh.epoch1.alpha(i,:) = subjectPAS.paper.ERCoh.epoch1.alpha;
    study.pas.ERCoh.epoch2.alpha(i,:) = subjectPAS.paper.ERCoh.epoch2.alpha;
    
    study.pas.ERCoh.epoch1.beta(i,:) = subjectPAS.paper.ERCoh.epoch1.beta;
    study.pas.ERCoh.epoch2.beta(i,:) = subjectPAS.paper.ERCoh.epoch2.beta;
    
    %tms 
    
    study.tms.ERPow.epoch1.alpha(i,:) = subjectTMS.paper.ERPow.epoch1.alpha;
    study.tms.ERPow.epoch2.alpha(i,:) = subjectTMS.paper.ERPow.epoch2.alpha;
    
    study.tms.ERPow.epoch1.beta(i,:) = subjectTMS.paper.ERPow.epoch1.beta;
    study.tms.ERPow.epoch2.beta(i,:) = subjectTMS.paper.ERPow.epoch2.beta;
    
    study.tms.ERCoh.epoch1.alpha(i,:) = subjectTMS.paper.ERCoh.epoch1.alpha;
    study.tms.ERCoh.epoch2.alpha(i,:) = subjectTMS.paper.ERCoh.epoch2.alpha;
    
    study.tms.ERCoh.epoch1.beta(i,:) = subjectTMS.paper.ERCoh.epoch1.beta;
    study.tms.ERCoh.epoch2.beta(i,:) = subjectTMS.paper.ERCoh.epoch2.beta;
end
%% pas erpow
channels = subjectPAS.paper.channels;
legStr = {study.subject(:).name, 'Mean'};

%ERPow - pas alpha
figure
subplot(321);
h1 = bar([study.pas.ERPow.epoch1.alpha(:,1:3)'  mean(study.pas.ERPow.epoch1.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.pas.ERPow.epoch1.alpha(:,4:6)'  mean(study.pas.ERPow.epoch1.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.pas.ERPow.epoch1.alpha(:,7:9)'  mean(study.pas.ERPow.epoch1.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.pas.ERPow.epoch2.alpha(:,1:3)'  mean(study.pas.ERPow.epoch2.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.pas.ERPow.epoch2.alpha(:,4:6)'  mean(study.pas.ERPow.epoch2.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.pas.ERPow.epoch2.alpha(:,7:9)'  mean(study.pas.ERPow.epoch2.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf PAS, Alpha band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERPow - pas beta 
figure
subplot(321);
h1 = bar([study.pas.ERPow.epoch1.beta(:,1:3)'  mean(study.pas.ERPow.epoch1.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.pas.ERPow.epoch1.beta(:,4:6)'  mean(study.pas.ERPow.epoch1.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.pas.ERPow.epoch1.beta(:,7:9)'  mean(study.pas.ERPow.epoch1.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.pas.ERPow.epoch2.beta(:,1:3)'  mean(study.pas.ERPow.epoch2.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.pas.ERPow.epoch2.beta(:,4:6)'  mean(study.pas.ERPow.epoch2.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.pas.ERPow.epoch2.beta(:,7:9)'  mean(study.pas.ERPow.epoch2.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf PAS, Beta band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)
%% pas ercoh

%ERCoh- pas alpha

figure

subplot(321);
h1 = bar([study.pas.ERCoh.epoch1.alpha(:,1:3)'  mean(study.pas.ERCoh.epoch1.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.pas.ERCoh.epoch1.alpha(:,4:6)'  mean(study.pas.ERCoh.epoch1.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.pas.ERCoh.epoch1.alpha(:,7:9)'  mean(study.pas.ERCoh.epoch1.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.pas.ERCoh.epoch2.alpha(:,1:3)'  mean(study.pas.ERCoh.epoch2.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.pas.ERCoh.epoch2.alpha(:,4:6)'  mean(study.pas.ERCoh.epoch2.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.pas.ERCoh.epoch2.alpha(:,7:9)'  mean(study.pas.ERCoh.epoch2.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf PAS, Alpha band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERCoh pas beta

figure

subplot(321);
h1 = bar([study.pas.ERCoh.epoch1.beta(:,1:3)'  mean(study.pas.ERCoh.epoch1.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis, 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.pas.ERCoh.epoch1.beta(:,4:6)'  mean(study.pas.ERCoh.epoch1.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis, 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.pas.ERCoh.epoch1.beta(:,7:9)'  mean(study.pas.ERCoh.epoch1.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.pas.ERCoh.epoch2.beta(:,1:3)'  mean(study.pas.ERCoh.epoch2.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.pas.ERCoh.epoch2.beta(:,4:6)'  mean(study.pas.ERCoh.epoch2.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.pas.ERCoh.epoch2.beta(:,7:9)'  mean(study.pas.ERCoh.epoch2.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf PAS, Beta band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%% tms ERpow

%ERpow alpha

figure
subplot(321);
h1 = bar([study.tms.ERPow.epoch1.alpha(:,1:3)'  mean(study.tms.ERPow.epoch1.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.tms.ERPow.epoch1.alpha(:,4:6)'  mean(study.tms.ERPow.epoch1.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.tms.ERPow.epoch1.alpha(:,7:9)'  mean(study.tms.ERPow.epoch1.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.tms.ERPow.epoch2.alpha(:,1:3)'  mean(study.tms.ERPow.epoch2.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.tms.ERPow.epoch2.alpha(:,4:6)'  mean(study.tms.ERPow.epoch2.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)], 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.tms.ERPow.epoch2.alpha(:,7:9)'  mean(study.tms.ERPow.epoch2.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf TMS, Alpha band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERPow - tms beta 
figure
subplot(321);
h1 = bar([study.tms.ERPow.epoch1.beta(:,1:3)'  mean(study.tms.ERPow.epoch1.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.tms.ERPow.epoch1.beta(:,4:6)'  mean(study.tms.ERPow.epoch1.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.tms.ERPow.epoch1.beta(:,7:9)'  mean(study.tms.ERPow.epoch1.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.tms.ERPow.epoch2.beta(:,1:3)'  mean(study.tms.ERPow.epoch2.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.tms.ERPow.epoch2.beta(:,4:6)'  mean(study.tms.ERPow.epoch2.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.tms.ERPow.epoch2.beta(:,7:9)'  mean(study.tms.ERPow.epoch2.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', powyaxis(1):25:powyaxis(2), 'ylim',[powyaxis(1) powyaxis(2)] , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf TMS, Beta band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)
%% tms ER coh
%ERCoh tms alpha

figure

subplot(321);
h1 = bar([study.tms.ERCoh.epoch1.alpha(:,1:3)'  mean(study.tms.ERCoh.epoch1.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.tms.ERCoh.epoch1.alpha(:,4:6)'  mean(study.tms.ERCoh.epoch1.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.tms.ERCoh.epoch1.alpha(:,7:9)'  mean(study.tms.ERCoh.epoch1.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.tms.ERCoh.epoch2.alpha(:,1:3)'  mean(study.tms.ERCoh.epoch2.alpha(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.tms.ERCoh.epoch2.alpha(:,4:6)'  mean(study.tms.ERCoh.epoch2.alpha(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.tms.ERCoh.epoch2.alpha(:,7:9)'  mean(study.tms.ERCoh.epoch2.alpha(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf TMS, Alpha band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERCoh tms beta

figure

subplot(321);
h1 = bar([study.tms.ERCoh.epoch1.beta(:,1:3)'  mean(study.tms.ERCoh.epoch1.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis, 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar([study.tms.ERCoh.epoch1.beta(:,4:6)'  mean(study.tms.ERCoh.epoch1.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis, 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar([study.tms.ERCoh.epoch1.beta(:,7:9)'  mean(study.tms.ERCoh.epoch1.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar([study.tms.ERCoh.epoch2.beta(:,1:3)'  mean(study.tms.ERCoh.epoch2.beta(:,1:3))'], 'grouped');
set(gca, 'Xticklabel', channels(1:3), 'ytick',cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar([study.tms.ERCoh.epoch2.beta(:,4:6)'  mean(study.tms.ERCoh.epoch2.beta(:,4:6))'], 'grouped');
set(gca, 'Xticklabel', channels(4:6), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar([study.tms.ERCoh.epoch2.beta(:,7:9)'  mean(study.tms.ERCoh.epoch2.beta(:,7:9))'], 'grouped');
set(gca, 'Xticklabel', channels(7:9), 'ytick', cohyaxis(1):0.05:cohyaxis(2), 'ylim',cohyaxis , 'ygrid', 'on','fontsize', 14);

leg = legend(legStr);
set(leg, 'orientation', 'horizontal');
set(leg, 'position', [0.0878    0.0044    0.8155    0.0714]);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf TMS, Beta band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)