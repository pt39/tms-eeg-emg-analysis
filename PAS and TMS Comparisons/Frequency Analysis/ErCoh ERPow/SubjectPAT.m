paths;
eeglab;

eegfolder = cat(2, DATAPATH, '\PAT\', 'EEG\');

%time regions to calculate metrics with
epochBaseline = [-0.530 -0.030];
epoch1 = [0.030 0.530] + .05;
epoch2 = [0.531 1.031] + .05;

%epochs to reject
fromBaseline = [ 4    25    98   132   263   312   325   344   350   359   370];
fromEpoch1 = [ 98   133   153   154   181   250   260   263   276   322   324   328   344   381 ];
fromEpoch2 = [  106   148   159   262   328];

rejectedEpochs = unique([fromBaseline, fromEpoch1, fromEpoch2]);

%%
fc = [0.01 512];  %filter edge frequencies before epoching
epochfc = [1 30]; %filter edge frequencies after epoching

%load pas (raw .cnt's)
EEG1 = pop_loadeep([eegfolder '20140227_1737.cnt'] , 'triggerfile', 'on');
EEG1 = preExtraction(EEG1); %remove the extraneous event the gives errors when using eeglab gui
EEG1 = pop_select(EEG1,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEG1 = pop_rmbase(EEG1, []); %remove baseline of all data
EEG1 = pop_reref(EEG1, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEG1.srate/2));
EEG1.data = filtfilt(B,A, double(EEG1.data'))'; %slightly filter the data

EEG2 = pop_loadeep([eegfolder '20140227_1758.cnt'] , 'triggerfile', 'on');
EEG2 = preExtraction(EEG2);
EEG2 = pop_select(EEG2,'nochannel',{'HEOG' 'VEOG'}); 
EEG2 = pop_rmbase(EEG2, []); 
EEG2 = pop_reref(EEG2, []); 
[B, A]  = butter(2,fc/(EEG2.srate/2));
EEG2.data = filtfilt(B,A, double(EEG2.data'))';

EEGPAS = pop_mergeset([EEG1 EEG2], [1 2]);
EEGPAS.setname = 'PAS';

fs = EEGPAS.srate;

[B, A]  = butter(2,epochfc/(EEGPAS.srate/2));

EEGPAS_baseline = pop_epoch(EEGPAS, {'2'}, epochBaseline);
EEGPAS_baseline = pop_select( EEGPAS_baseline,'notrial',rejectedEpochs );
EEGPAS_baseline.setname = 'PAS Baseline';
EEGPAS_baseline = filterEpochs(B,A,EEGPAS_baseline);

EEGPAS_epoch1 = pop_epoch(EEGPAS, {'2'}, epoch1);
EEGPAS_epoch1 = pop_select( EEGPAS_epoch1,'notrial',rejectedEpochs );
EEGPAS_epoch1.setname = 'PAS Epoch 1';
EEGPAS_epoch1= filterEpochs(B,A,EEGPAS_epoch1);

EEGPAS_epoch2 = pop_epoch(EEGPAS, {'2'}, epoch2);
EEGPAS_epoch2 = pop_select( EEGPAS_epoch2,'notrial',rejectedEpochs );
EEGPAS_epoch2.setname = 'PAS Epoch 2';
EEGPAS_epoch2= filterEpochs(B,A,EEGPAS_epoch2);

fprintf('Number of trials kept: %d\n', size(EEGPAS_baseline.data, 3));

[ALLEEG EEG] = eeg_store(ALLEEG, [EEGPAS_baseline, EEGPAS_epoch1, EEGPAS_epoch2]);
eeglab redraw
%%
%% Spectra calculation

%fft was computed for all electrodes and then averaged across epochs under
%the same conditions

pxx.baseline = [];
pxx.epoch1 = [];
pxx.epoch2 = [];

pxy.baseline = [];
pxy.epoch1 = [];
pxy.epoch2 = [];

seedChannel = findChanLoc(EEGPAS, 'C3');

a=[];
for k = 1:size(EEGPAS_baseline.data,3)
    for sp=1:length(a), fprintf('\b'); end
    a = sprintf('Epoch %d/%d', k,size(EEGPAS_baseline.data,3));
    fprintf(a);
    
    seedbaseline = EEGPAS_baseline.data(seedChannel,:, k);
    seedepoch1 = EEGPAS_epoch1.data(seedChannel,:, k);
    seedepoch2 = EEGPAS_epoch2.data(seedChannel,:, k);
    
    for i =  1:size(EEGPAS_baseline.data, 1)
        currbaseline = EEGPAS_baseline.data(i,:, k);
        currepoch1 = EEGPAS_epoch1.data(i,:, k);
        currepoch2 = EEGPAS_epoch2.data(i,:, k);
        
        %PXX
        
        [pxx.baseline(i,:,k), ~] = periodogram(currbaseline, hamming(length(currbaseline)),fs*2, fs); 
        [pxx.epoch1(i,:,k), ~] = periodogram(currepoch1, hamming(length(currepoch1)), fs*2, fs ); 
        [pxx.epoch2(i,:,k), f] = periodogram(currepoch2, hamming(length(currepoch2)),fs*2, fs ); 
        
        %PXY

        [pxy.baseline(i,:,k), ~] = cpsd(seedbaseline, currbaseline, hamming(length(currbaseline)), 0, fs*2, fs);
        [pxy.epoch1(i,:,k), ~] = cpsd(seedepoch1, currepoch1, hamming(length(currepoch1)), 0, fs*2, fs);
        [pxy.epoch2(i,:,k), ~] = cpsd(seedepoch2, currepoch2, hamming(length(currepoch2)), 0, fs*2, fs);
        
    end
end
fprintf('\nDone\n');

%% outliear detection

% c3 = squeeze(pxx.baseline(15, :, :));
% c3 = max(c3);
% disp(find(abs(c3) > mean(c3) + 2*std(c3)))
% c3 = squeeze(pxx.epoch1(15, :, :));
% c3 = max(c3);
% disp(find(abs(c3) > mean(c3) + 2*std(c3)))
% c3 = squeeze(pxx.epoch2(15, :, :));
% c3 = max(c3);
% disp(find(abs(c3) > mean(c3) + 2*std(c3)))

%%

subplot(131);
plotSpectra(pxx.baseline, f, findChanLoc(EEGPAS,'C3'));
title('Baseline Spectra');
xlim([0 30]);
ylim([0 15]);

subplot(132);
plotSpectra(pxx.epoch1, f, findChanLoc(EEGPAS,'C3'));
title('Epoch 1 Spectra');
xlim([0 30]);
ylim([0 15]);

subplot(133);
plotSpectra(pxx.epoch2, f, findChanLoc(EEGPAS,'C3'));
title('Epoch 2 Spectra');
xlim([0 30]);
ylim([0 15]);

%% ERPow
alpharange = find(f >= 8 & f <=12);
betarange = find(f >= 13 & f <=30);


baseline.spectrum = mean(pxx.baseline,3);
epoch1.spectrum = mean(pxx.epoch1,3);
epoch2.spectrum = mean(pxx.epoch2,3);

for i = 1:size(baseline.spectrum,1)
    baseline.alpha(i) = mean(baseline.spectrum(i,alpharange));%trapz(f(alpharange), baseline.spectrum(i,alpharange));
    baseline.beta(i) =  mean(baseline.spectrum(i,betarange));%trapz(f(betarange), baseline.spectrum(i,betarange));

    epoch1.alpha(i) = mean(epoch1.spectrum(i,alpharange));%trapz(f(alpharange), epoch1.spectrum(i,alpharange));
    epoch1.beta(i) = mean(epoch1.spectrum(i,betarange));%trapz(f(betarange), epoch1.spectrum(i,betarange));

    epoch2.alpha(i) = mean(epoch2.spectrum(i,alpharange));% trapz(f(alpharange), epoch2.spectrum(i,alpharange));
    epoch2.beta(i) = mean(epoch2.spectrum(i,betarange));% trapz(f(betarange), epoch2.spectrum(i,betarange));
    
    ERPow.epoch1.alpha(i) = (epoch1.alpha(i) - baseline.alpha(i))/baseline.alpha(i) * 100;
    ERPow.epoch1.beta(i) = (epoch1.beta(i) - baseline.beta(i))/baseline.beta(i) * 100;

    ERPow.epoch2.alpha(i) = (epoch2.alpha(i) - baseline.alpha(i))/baseline.alpha(i) * 100;
    ERPow.epoch2.beta(i) = (epoch2.beta(i) - baseline.beta(i))/baseline.beta(i) * 100;
end
%%

EEGPAS=pop_chanedit(EEGPAS, 'lookup',[ WD '\standard-10-5-cap385.elp'] );

figure;

subplot(121);
title('Alpha Epoch 1 ERPow')
topoplot(ERPow.epoch1.alpha, EEGPAS.chanlocs);
caxis([-30 30])
colorbar

subplot(122);
title('Alpha Epoch 2 ERPow')
topoplot(ERPow.epoch2.alpha,EEGPAS.chanlocs);
caxis([-30 30])
colorbar

figure

subplot(121);
title('Beta Epoch 1 ERPow')
topoplot(ERPow.epoch1.beta, EEGPAS.chanlocs);
caxis([-30 30])
colorbar

subplot(122);
title('Beta Epoch 2 ERPow')
topoplot(ERPow.epoch2.beta,EEGPAS.chanlocs);
caxis([-30 30])
colorbar

%% ERCoh
seedChannel = findChanLoc(EEGPAS,'C3');
%for average reference

baseline.spectrum = mean(pxx.baseline,3);
epoch1.spectrum = mean(pxx.epoch1,3);
epoch2.spectrum = mean(pxx.epoch2,3);

baseline.crossspectrum = mean(pxy.baseline,3);
epoch1.crossspectrum = mean(pxy.epoch1,3);
epoch2.crossspectrum = mean(pxy.epoch2,3);

baseline.coherence.alpha = (abs(baseline.crossspectrum(:, alpharange)).^2) ./ (abs(baseline.spectrum(:, alpharange)).*abs(baseline.spectrum(:, alpharange)));
baseline.coherence.alpha = mean(baseline.coherence.alpha,2);

epoch1.coherence.alpha = (abs(epoch1.crossspectrum(:, alpharange)).^2) ./ (abs(epoch1.spectrum(:, alpharange)).*abs(epoch1.spectrum(:, alpharange)));
epoch1.coherence.alpha = mean(epoch1.coherence.alpha,2);

epoch2.coherence.alpha = (abs(epoch2.crossspectrum(:, alpharange)).^2) ./ (abs(epoch2.spectrum(:, alpharange)).*abs(epoch2.spectrum(:, alpharange)));
epoch2.coherence.alpha = mean(epoch2.coherence.alpha,2);


baseline.coherence.beta = (abs(baseline.crossspectrum(:, betarange)).^2) ./ (abs(baseline.spectrum(:, betarange)).*abs(baseline.spectrum(:, betarange)));
baseline.coherence.beta = mean(baseline.coherence.beta,2);

epoch1.coherence.beta = (abs(epoch1.crossspectrum(:, betarange)).^2) ./ (abs(epoch1.spectrum(:, betarange)).*abs(epoch1.spectrum(:, betarange)));
epoch1.coherence.beta = mean(epoch1.coherence.beta,2);

epoch2.coherence.beta = (abs(epoch2.crossspectrum(:, betarange)).^2) ./ (abs(epoch2.spectrum(:, betarange)).*abs(epoch2.spectrum(:, betarange)));
epoch2.coherence.beta = mean(epoch2.coherence.beta,2);

ERCoh.epoch1.alpha = epoch1.coherence.alpha - baseline.coherence.alpha;
ERCoh.epoch1.beta = epoch1.coherence.beta - baseline.coherence.beta;

ERCoh.epoch2.alpha = epoch2.coherence.alpha - baseline.coherence.alpha;
ERCoh.epoch2.beta = epoch2.coherence.beta - baseline.coherence.beta;

% 
% baseline.coherence.alpha = corrcoef(baseline.spectrum(:,alpharange)');
% baseline.coherence.alpha = baseline.coherence.alpha(seedChannel, :).^2;
%     
% epoch1.coherence.alpha = corrcoef(epoch1.spectrum(:,alpharange)');
% epoch1.coherence.alpha = epoch1.coherence.alpha(seedChannel, :).^2;
% 
% epoch2.coherence.alpha = corrcoef(epoch2.spectrum(:,alpharange)');
% epoch2.coherence.alpha = epoch2.coherence.alpha(seedChannel, :).^2;
% 
% baseline.coherence.beta = corrcoef(baseline.spectrum(:,betarange)');
% baseline.coherence.beta = baseline.coherence.beta(seedChannel, :).^2;
%     
% epoch1.coherence.beta = corrcoef(epoch1.spectrum(:,betarange)');
% epoch1.coherence.beta = epoch1.coherence.beta(seedChannel, :).^2;
% 
% epoch2.coherence.beta = corrcoef(epoch2.spectrum(:,betarange)');
% epoch2.coherence.beta = epoch2.coherence.beta(seedChannel, :).^2;


%%

figure;

subplot(121);
title('Alpha Epoch 1 ERCoh')
topoplot(ERCoh.epoch1.alpha, EEGPAS.chanlocs);
caxis([-0.25 0.25])
colorbar

subplot(122);
title('Alpha Epoch 2 ERCoh')
topoplot(ERCoh.epoch2.alpha,EEGPAS.chanlocs);
caxis([-0.25 0.25])
colorbar

figure;

subplot(121);
title('Beta Epoch 1 ERCoh')
topoplot(ERCoh.epoch1.beta, EEGPAS.chanlocs);
caxis([-0.25 0.25])
colorbar

subplot(122);
title('Beta Epoch 2 ERCoh')
topoplot(ERCoh.epoch2.beta,EEGPAS.chanlocs);
caxis([-0.25 0.25])
colorbar

%% replicate plots from paper
paper.channels = {'F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'};

for i = 1:length(paper.channels)
    dchan = findChanLoc(EEGPAS,paper.channels{i});
    
    paper.ERPow.epoch1.alpha(i) = ERPow.epoch1.alpha(dchan);
    paper.ERPow.epoch1.beta(i) = ERPow.epoch1.beta(dchan);

    paper.ERPow.epoch2.alpha(i) = ERPow.epoch2.alpha(dchan);
    paper.ERPow.epoch2.beta(i) = ERPow.epoch2.beta(dchan);
    
    paper.ERCoh.epoch1.alpha(i) = ERCoh.epoch1.alpha(dchan);
    paper.ERCoh.epoch1.beta(i) = ERCoh.epoch1.beta(dchan);
    
    paper.ERCoh.epoch2.alpha(i) = ERCoh.epoch2.alpha(dchan);
    paper.ERCoh.epoch2.beta(i) = ERCoh.epoch2.beta(dchan);
end

%ERPow - alpha
figure
subplot(321);
h1 = bar(paper.ERPow.epoch1.alpha(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar(paper.ERPow.epoch1.alpha(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar(paper.ERPow.epoch1.alpha(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar(paper.ERPow.epoch2.alpha(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar(paper.ERPow.epoch2.alpha(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar(paper.ERPow.epoch2.alpha(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf Subject PAT PAS, Alpha band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERPow beta

figure

subplot(321);
h1 = bar(paper.ERPow.epoch1.beta(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar(paper.ERPow.epoch1.beta(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar(paper.ERPow.epoch1.beta(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar(paper.ERPow.epoch2.beta(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar(paper.ERPow.epoch2.beta(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar(paper.ERPow.epoch2.beta(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -40:10:80, 'ylim',[-40 80] , 'ygrid', 'on','fontsize', 14);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf Subject PAT PAS, Beta band event-related power','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERCoh alpha

figure

subplot(321);
h1 = bar(paper.ERCoh.epoch1.alpha(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar(paper.ERCoh.epoch1.alpha(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar(paper.ERCoh.epoch1.alpha(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar(paper.ERCoh.epoch2.alpha(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar(paper.ERCoh.epoch2.alpha(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar(paper.ERCoh.epoch2.alpha(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -0.1:0.05:0.2, 'ylim',[-0.1 0.2] , 'ygrid', 'on','fontsize', 14);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf Subject PAT PAS, Alpha band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

%ERCoh beta

figure

subplot(321);
h1 = bar(paper.ERCoh.epoch1.beta(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on', 'fontsize', 14);
title('First Epoch');
subplot(323);
h2 = bar(paper.ERCoh.epoch1.beta(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on','fontsize', 14);
subplot(325);
h3 = bar(paper.ERCoh.epoch1.beta(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on','fontsize', 14);

subplot(322);
h1 = bar(paper.ERCoh.epoch2.beta(1:3)', 'grouped');
set(gca, 'Xticklabel', paper.channels(1:3), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on', 'fontsize', 14);
title('Second Epoch');
subplot(324);
h2 = bar(paper.ERCoh.epoch2.beta(4:6)', 'grouped');
set(gca, 'Xticklabel', paper.channels(4:6), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on','fontsize', 14);
subplot(326);
h3 = bar(paper.ERCoh.epoch2.beta(7:9)', 'grouped');
set(gca, 'Xticklabel', paper.channels(7:9), 'ytick', -0.05:0.025:0.1, 'ylim',[-0.05 0.1] , 'ygrid', 'on','fontsize', 14);

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,'\bf Subject PAT PAS, Beta band event-related coherence','HorizontalAlignment','center','VerticalAlignment', 'top', 'fontsize', 20)

save('subjectPAT', 'paper');
