function EEGnew = filterEpochs(B,A, EEGold)

    EEGnew = EEGold;
    for i = 1:size(EEGold.data, 3)
        EEGnew.data(:,:,i) = filtfilt(B,A, double(EEGold.data(:,:,i)'))';
    end
    
end