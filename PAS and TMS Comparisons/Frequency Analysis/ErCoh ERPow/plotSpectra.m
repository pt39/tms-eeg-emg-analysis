function plotSpectra(spectra,f, dchan)
    
    for k = 1:size(spectra,3)
           plot(f,spectra(dchan,:, k), 'k') 
           hold on;
    end
    plot(f,mean(spectra(dchan,:, :),3), 'g') 
    
    xlabel('Frequency (Hz)');
    ylabel('Power Spectral Density (\muV^2/Hz)');
   %safdasdfsa   
end