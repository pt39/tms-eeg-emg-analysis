paths;
eeglab

eegfolder = cat(2, DATAPATH, '\PAT\', 'EEG\');
epochrange = [-0.015 1.2];
fc = [0.1 500]; %this will the high pass cut off

%load pas (raw .cnt's)
%load pas (raw .cnt's)
EEG1 = pop_loadeep([eegfolder '20140227_1652.cnt'] , 'triggerfile', 'on');
EEG1 = preExtraction(EEG1); %remove the extraneous event the gives errors when using eeglab gui
EEG1 = pop_select(EEG1,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEG1 = pop_rmbase(EEG1, []); %remove baseline of all data
EEG1 = pop_reref(EEG1, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEG1.srate/2));
EEG1.data = filtfilt(B,A, double(EEG1.data'))'; %slightly filter the data
EEG1 = pop_epoch(EEG1, {'2'}, epochrange);


ALLEEG = eeg_store(ALLEEG, EEGTMS);

eeglab redraw