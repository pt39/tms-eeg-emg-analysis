paths;
eeglab

eegfolder = cat(2, DATAPATH, '\MS\', 'EEG files\');
epochrange = [-0.015 1.2];
fc = [0.1 500]; %this will the high pass cut off

%load pas (raw .cnt's)
EEGTMS = pop_loadeep([eegfolder 'MSTMSonly.cnt'] , 'triggerfile', 'on');
EEGTMS = preExtraction(EEGTMS); %remove the extraneous event the gives errors when using eeglab gui
EEGTMS = pop_select(EEGTMS,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEGTMS = pop_rmbase(EEGTMS, []); %remove baseline of all data
EEGTMS = pop_reref(EEGTMS, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEGTMS.srate/2));
EEGTMS.data = filtfilt(B,A, double(EEGTMS.data'))'; %slightly filter the data
EEGTMS.setname = 'TMS';

ALLEEG = eeg_store(ALLEEG, EEGTMS);