%% NBW pas
% it won't run ICA again if MSpascomps is there
paths;

ft_defaults

eegfolder = cat(2, DATAPATH, '\NBW\', 'EEG files\');
filepath = [eegfolder 'NBW_PAS.cnt'];
%tms
cfg = [];
cfg.dataset                 = filepath;
cfg.continuous              = 'yes';
cfg.trialdef.prestim        = 0.5;%number, latency in seconds (optional)
cfg.trialdef.poststim       = 1.5; %number, latency in seconds (optional)
cfg.trialdef.eventtype      = 'trigger';
cfg.trialdef.eventvalue     = 2;
cfg = ft_definetrial(cfg);
%%
trl = cfg.trl;
%%
cfg.channel     = {'all', '-M1', '-M2', '-HEOG', '-VEOG'};
cfg.reref       = 'yes';
cfg.refchannel  = {'all'};
data_tms = ft_preprocessing(cfg);

%%

cfg = [];
cfg.preproc.demean = 'yes';
cfg.preproc.baselinewindow = [-0.1 -0.001];
ft_databrowser(cfg, data_tms);
%%
cfg = [];
cfg.preproc.demean = 'yes';
cfg.preproc.baselinewindow = [-0.1 -0.001];
data_tms_avg = ft_timelockanalysis(cfg, data_tms);
%%
channel = 'C3';
 
figure;
channel_idx = find(strcmp(channel, data_tms_avg.label));
plot(data_tms_avg.time, data_tms_avg.avg(channel_idx,:));  % Plot all data
xlim([-0.1 0.6]);    % Here we can specify the limits of what to plot on the x-axis
ylim([-60 100]);     % Here we can specify the limits of what to plot on the y-axis
title(['Channel ' data_tms_avg.label{channel_idx}]);
ylabel('Amplitude (uV)')
xlabel('Time (s)');

hold on; % Plotting new data does not remove old plot
 
% Specify time-ranges to higlight
ringing = [-0.0002 0.0044];
muscle  = [0.0044 0.015];
decay   = [0.0150 0.200];
 
colors = 'rgc';
labels = {'ringing','muscle','decay'};
artifacts = [ringing; muscle; decay];
 
for i=1:numel(labels)
  highlight_idx = [nearest(data_tms_avg.time,artifacts(i,1)) nearest(data_tms_avg.time,artifacts(i,2)) ];
  plot(data_tms_avg.time(highlight_idx(1):highlight_idx(2)), data_tms_avg.avg(channel_idx,highlight_idx(1):highlight_idx(2)),colors(i));
end;
legend(['raw data', labels]);

%%
cfg                         = [];
cfg.dataset                 = filepath;
cfg.continuous              = 'no'; % The alternative is 'detect' to detect the onset of pulses
cfg.prestim                 = .001;     % First time-point of range to exclude
cfg.poststim                = .006;     % Last time-point of range to exclude
cfg.method                  = 'marker';
cfg.trialdef.eventtype      = 'trigger';
cfg.trialdef.eventvalue     = 2;
cfg_ringing = ft_artifact_tms(cfg);  

%%

cfg_artifact = [];
cfg_artifact.dataset = filepath;
cfg_artifact.artfctdef.ringing.artifact = cfg_ringing.artfctdef.tms.artifact; % Add ringing/step response artifact definition

%%
cfg_artifact.artfctdef.reject = 'partial'; % Can also be 'complete', or 'nan';
cfg_artifact.trl = trl; % We supply ft_rejectartifact with the original trial structure so it nows where to look for artifacts.
cfg_artifact.artfctdef.minaccepttim = 0.01; % This specifies the minimumm size of resulting trials. You have to set this, the default is too large for thre present data, resulting in small artifact-free segments being rejected as well.
cfg = ft_rejectartifact(cfg_artifact); % Reject trials partially
%%
cfg.channel     = {'all', '-M1', '-M2', '-HEOG', '-VEOG'};
cfg.reref       = 'yes';
cfg.refchannel  = {'all'};
data_tms_clean  = ft_preprocessing(cfg);
%%

% Browse through the segmented data
cfg = [];
cfg.artfctdef = cfg_artifact.artfctdef; % Store previously obtained artifact definition
cfg.continuous = 'yes'; % Setting this to yes forces ft_databrowser to represent our segmented data as one continuous signal
ft_databrowser(cfg, data_tms_clean);

%% Perform ICA on segmented data
if(exist('NBWpascomps.mat','file') ~=2 )
    cfg = [];
    cfg.demean = 'yes'; 
    cfg.method = 'fastica';        % FieldTrip supports multiple ways to perform ICA, 'fastica' is one of them.
    cfg.fastica.approach = 'symm'; % All components will be estimated simultaneously.
    cfg.fastica.g = 'gauss'; 

    comp = ft_componentanalysis(cfg, data_tms_clean);
 
    save('NBWpascomps','comp','-v7.3');
    
else
    load('NBWpascomps');
end
   beep();pause(1);beep();pause(1);beep();
%%
cfg = [];
cfg.vartrllength  = 2; % This is necessary as our trials are in fact segments of our original trials. This option tells the function to reconstruct the original trials based on the sample-information stored in the data
comp_avg = ft_timelockanalysis(cfg, comp);

%%
% 
% figure;
% cfg = [];
% cfg.layout    = 'eeg1005.lay';
% ft_databrowser(cfg, comp_avg);

%%
figure;
cfg           = [];
cfg.component = [1:61];
cfg.comment   = 'no';
cfg.layout    = 'eeg1005.lay'; % If you use a function that requires plotting of topographical information you need to supply the function with the location of your channels
ft_topoplotIC(cfg, comp);

%%
figure;
cfg           = [];
cfg.component = [4 6 10 31 11 33 44 43]; 
cfg.comment   = 'no';
cfg.layout    = 'eeg1005.lay'; % If you use a function that requires plotting of topographical information you need to supply the function with the location of your channels
ft_topoplotIC(cfg, comp);

figure
colors = 'brgkmy';
xlim([-0.3 1.5]);

hold on
% for i = 1:61
%     subplot(121);
%     plot(comp_avg.time, comp_avg.avg(i,:), colors(6-mod(i, 6)));ylim([-50 50]);
%     title(num2str(i));
%     subplot(122)
%     cfg           = [];
%     cfg.component = i ; 
%     cfg.comment   = 'no';
%     cfg.layout    = 'eeg1005.lay'; % If you use a function that requires plotting of topographical information you need to supply the function with the location of your channels
%     ft_topoplotIC(cfg, comp);
%   	hold off
%     pause
% end

%%
figure
colors = 'brgkmy';
xlim([-0.3 1.5]);

hold on
for i = cfg.component 
    plot(comp_avg.time, comp_avg.avg(i,:), colors(6-mod(i, 6)));ylim([-50 50]);
    title(num2str(i));
  	hold on
end

%%


    cfg          = [];
    cfg.demean   = 'no'; % This has to be explicitly stated as the default is to demean.
    cfg.unmixing = comp.unmixing; % Supply the matrix necessay to 'unmix' the channel-series data into components
    cfg.topolabel = comp.topolabel; % Supply the original channel label information

    comp          = ft_componentanalysis(cfg, data_tms_clean);

    


%%
cfg            = [];
cfg.component = [4 6 10 31 11 33 44 43];
cfg.demean     = 'no';
 
data_tms_clean2 = ft_rejectcomponent(cfg, comp);

%%

cfg                = [];
cfg.vartrllength   = 2;
cfg.preproc.demean = 'yes'; % Demeaning is still applied on the segments of the trials, rather than the entire trial. To avoid offsets within trials, set this to 'no'
cfg.preproc.baselinewindow = [-0.05 -0.001];
 
data_tms_clean_avg = ft_timelockanalysis(cfg, data_tms_clean2);
 
% Plot all channels 
figure;
subplot(121)
plot(data_tms_avg.time, data_tms_avg.avg(15,:),'r');
hold on
plot(data_tms_clean_avg.time, data_tms_clean_avg.avg(15,:),'b'); % Plot all data
xlim([-0.1 0.6]); % Here we can specify the limits of what to plot on the x-axis
ylim([-30 30]);
title(['Channel ' data_tms_clean_avg.label{15}]);
ylabel('Amplitude (uV)')
xlabel('Time (s)');

legend({'Raw' 'Cleaned'});

subplot(122)
plot(data_tms_avg.time, data_tms_avg.avg(14,:),'r');
hold on
plot(data_tms_clean_avg.time, data_tms_clean_avg.avg(14,:),'b'); % Plot all data
xlim([-0.1 0.6]); % Here we can specify the limits of what to plot on the x-axis
ylim([-30 30]);
title(['Channel ' data_tms_clean_avg.label{14}]);
ylabel('Amplitude (uV)')
xlabel('Time (s)');

legend({'Raw' 'Cleaned'});

%%
cfg     = [];
cfg.trl = trl;
data_tms_clean2 = ft_redefinetrial(cfg, data_tms_clean2); % Restructure cleaned data

%%

muscle_window = [0.006 0.015]; % The window we would like to replace with nans
muscle_window_idx = [nearest(data_tms_clean2.time{1},muscle_window(1)) nearest(data_tms_clean2.time{1},muscle_window(2))]; % Find the indices in the time vector corresponding to our window of interest
for i=1:numel(data_tms_clean2.trial) % Loop through all trials
  data_tms_clean2.trial{i}(:,muscle_window_idx(1):muscle_window_idx(2))=nan; % Replace the segment of data corresponding to our window of interest with nans
end;
%%
% Interpolate nans using cubic interpolation
cfg = [];
cfg.method = 'cubic'; % Here you can specify any method that is supported by interp1: 'nearest','linear','spline','pchip','cubic','v5cubic'
cfg.prewindow = 0.01; % Window prior to segment to use data points for interpolation
cfg.postwindow = 0.01; % Window after segment to use data points for interpolation
data_tms_clean2 = ft_interpolatenan(cfg, data_tms_clean2); % Clean data
%%

cfg = [];
cfg.bpfilter = 'yes';
cfg.bpfreq = [0.5 80];
data_tms_clean2 = ft_preprocessing(cfg, data_tms_clean2);

%notch

cfg = [];
cfg.bsfilter = 'yes';
cfg.bsfreq = [55 65];
data_tms_clean2 = ft_preprocessing(cfg, data_tms_clean2);

%%
cfg = [];
cfg.preproc.demean = 'yes';
cfg.layout    = 'eeg1005.lay';
cfg.preproc.baselinewindow = [-0.05 -0.001];
cfg.xlim = [-0.03 0.6];
cfg.ylim = [-20 20];
data_tms_clean_avg = ft_timelockanalysis(cfg, data_tms_clean2);
for i=[14 15] % Loop through all channels
    figure;
    plot(data_tms_avg.time, data_tms_avg.avg(i,:),'r'); % Plot all data
    hold on;
    plot(data_tms_clean_avg.time, data_tms_clean_avg.avg(i,:),'b'); % Plot all data
    xlim([-0.03 0.4]); % Here we can specify the limits of what to plot on the x-axis
    ylim([-30 30]); % Here we can specify the limits of what to plot on the y-axis
    title(['Channel ' data_tms_avg.label{i}]);
    ylabel('Amplitude (\muV)')
    xlabel('Time (s)');
    legend({'Raw' 'Cleaned'});
end;

save('NBWpaserp', 'data_tms_clean_avg','-v7.3');
%%

cfg = [];
cfg.showlabels = 'yes'; 
cfg.fontsize = 6; 
cfg.layout = 'EEG1010_prasad.lay';
cfg.ylim = [-20 20];
 cfg.xlim = [-0.2 0.6];
 figure
ft_multiplotER(cfg, data_tms_clean_avg,data_tms_avg); 
