paths;
eeglab;
eegfolder = cat(2, DATAPATH, '\PAT\', 'EEG\');
epochrange = [-0.05 0.5];
fc = [0.1 500]; %this will the high pass cut off

%load pas (raw .cnt's)
EEG1 = pop_loadeep([eegfolder '20140227_1737.cnt'] , 'triggerfile', 'on');
EEG1 = preExtraction(EEG1); %remove the extraneous event the gives errors when using eeglab gui
EEG1 = pop_select(EEG1,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEG1 = pop_rmbase(EEG1, []); %remove baseline of all data
EEG1 = pop_reref(EEG1, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEG1.srate/2));
EEG1.data = filtfilt(B,A, double(EEG1.data'))'; %slightly filter the data

EEG2 = pop_loadeep([eegfolder '20140227_1758.cnt'] , 'triggerfile', 'on');
EEG2 = preExtraction(EEG2);
EEG2 = pop_select(EEG2,'nochannel',{'HEOG' 'VEOG'}); 
EEG2 = pop_rmbase(EEG2, []); 
EEG2 = pop_reref(EEG2, []); 
[B, A]  = butter(2,fc/(EEG2.srate/2));
EEG2.data = filtfilt(B,A, double(EEG2.data'))';

ALLEEG = eeg_store(ALLEEG, [EEG1, EEG2]);
eeglab redraw


eegChans = [findChanLoc(EEG1, 'C3'), findChanLoc(EEG1, 'Cz')];
eegpas1 = pop_tmsclean(EEG1,eegChans, 1.000000, 100.000000, 60.000000, {'2'}, 0.1000, 0.015000, 0.100000);
eegpas2 = pop_tmsclean(EEG2,eegChans, 1.000000, 100.000000, 60.000000, {'2'}, 0.1000, 0.015000, 0.100000);


EEGPAS = pop_mergeset(eegpas1, eegpas2);
EEGPAS = pop_epoch(EEGPAS,{'2'}, epochrange);
ALLEEG = eeg_store(ALLEEG, EEGPAS);
eeglab redraw

subplot(121);
plot(EEGPAS.times,mean(EEGPAS.data(findChanLoc(EEGPAS, 'C3'),:,:),3))
ylim([-40 40]);
title('C3');
xlim([EEGPAS.times(1), EEGPAS.times(end)]);

subplot(122)
plot(EEGPAS.times,mean(EEGPAS.data(findChanLoc(EEGPAS, 'Cz'),:,:),3))
ylim([-40 40]);
title('Cz');
xlim([EEGPAS.times(1), EEGPAS.times(end)]);