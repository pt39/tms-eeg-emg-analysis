function dchan = findChanLoc(EEG, chan)
    dchan = -1;
    for i = 1:length(EEG.chanlocs)
       if(strcmp(EEG.chanlocs(i).labels, chan))
           dchan = i;
           
       end
    end

end