paths;
eeglab;

eegfolder = cat(2, DATAPATH, '\SVA\', 'EEG files\');
epochrange = [-0.05 3];
fc = [0.1 500]; %this will the high pass cut off

%load pas (raw .cnt's)
EEGPAS = pop_loadeep([eegfolder 'SVApas_correct.cnt'] , 'triggerfile', 'on');
EEGPAS = preExtraction(EEGPAS); %remove the extraneous event the gives errors when using eeglab gui
EEGPAS = pop_select(EEGPAS,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEGPAS = pop_rmbase(EEGPAS, []); %remove baseline of all data
EEGPAS = pop_reref(EEGPAS, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEGPAS.srate/2));
EEGPAS.data = filtfilt(B,A, double(EEGPAS.data'))'; %slightly filter the data
EEGPAS = pop_epoch(EEGPAS, {'2'}, epochrange);
EEGPAS.setname = 'PAS';

ALLEEG = eeg_store(ALLEEG, EEGPAS);

eeglab redraw

subplot(121)
plotPCTEP(EEGPAS, [1 5 10 15 20 30], {'b', 'r', 'g', 'k', ':b', ':r'}, findChanLoc(EEGPAS,'C3'), -0.05);
ylim([-40 40]);
legend off;
subplot(122)
plotPCTEP(EEGPAS, [1 5 10 15 20 30], {'b', 'r', 'g', 'k', ':b', ':r'}, findChanLoc(EEGPAS,'Cz'), -0.05);
ylim([-40 40]);