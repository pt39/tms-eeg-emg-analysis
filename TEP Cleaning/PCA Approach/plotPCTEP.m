function plotPCTEP(EEG, vecnCompsToRemove, colors, dchan, epstart)
    EEG = pop_reref( EEG, []);

    erp = mean(EEG.data(dchan, :, :), 3);
    erpt = ((1:length(erp))/EEG.srate)+epstart;
    plot(erpt*1000, erp,'k');
    hold on
    [B,A] = butter(2, 150/(EEG.srate/2));
    for i = 1:length(vecnCompsToRemove)
        fprintf('Removing %d PCs\n', vecnCompsToRemove(i));

        nCompsToRemove = vecnCompsToRemove(i);

        epochs = zeros(size(EEG, 3), length(erpt));
        for j = 1:size(EEG.data,3)
            currentEpoch = EEG.data(:,:,j);
            [PCS] = pca(currentEpoch');
            U = PCS(:, (nCompsToRemove+1):end); % remove the first nCompsToRemove components
            correctedEpoch = U*U'*currentEpoch;

            correctedEpoch = filtfilt(B,A, double(correctedEpoch)')';
            epochs(j, :) = correctedEpoch(dchan,:);
        end
            plot(erpt*1000, mean(epochs), colors{i});
    end
    
    legstr{1} = 'raw';
    for i =1:length(vecnCompsToRemove)
        legstr{i+1} = cat(2, num2str(vecnCompsToRemove(i)), ' PCs');
    end
    legend(legstr);
    ylabel('Amplitude(\muV)');
    xlabel('Time(ms)');
    title(EEG.chanlocs(dchan).labels);
    xlim([epstart*1000 120]);
    ylim([-20 20]);