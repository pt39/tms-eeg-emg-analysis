paths;
eeglab;

eegfolder = cat(2, DATAPATH, '\PAT\', 'EEG\');
epochrange = [-0.015 1.2];
fc = [0.1 500]; %this will the high pass cut off

 
%load pas (raw .cnt's)
EEG1 = pop_loadeep([eegfolder '20140227_1737.cnt'] , 'triggerfile', 'on');
EEG1 = preExtraction(EEG1); %remove the extraneous event the gives errors when using eeglab gui
EEG1 = pop_select(EEG1,'nochannel',{'HEOG' 'VEOG'}); %remove HEOG and VEOG
EEG1 = pop_rmbase(EEG1, []); %remove baseline of all data
EEG1 = pop_reref(EEG1, []); %convert to common average reference
[B, A]  = butter(2,fc/(EEG1.srate/2));
EEG1.data = filtfilt(B,A, double(EEG1.data'))'; %slightly filter the data
EEG1 = pop_epoch(EEG1, {'2'}, epochrange);

EEG2 = pop_loadeep([eegfolder '20140227_1758.cnt'] , 'triggerfile', 'on');
EEG2 = preExtraction(EEG2);
EEG2 = pop_select(EEG2,'nochannel',{'HEOG' 'VEOG'}); 
EEG2 = pop_rmbase(EEG2, []); 
EEG2 = pop_reref(EEG2, []); 
[B, A]  = butter(2,fc/(EEG2.srate/2));
EEG2.data = filtfilt(B,A, double(EEG2.data'))';
EEG2 = pop_epoch(EEG2, {'2'}, epochrange);

EEGPAS = pop_mergeset([EEG1 EEG2], [1 2]);
EEGPAS.setname = 'PAS';

ALLEEG = eeg_store(ALLEEG, EEGPAS);

eeglab redraw

%plot values
subplot(121)
plotPCTEP(EEGPAS, [1 5 10 15 20 30], {'b', 'r', 'g', 'k', ':b', ':r'}, findChanLoc(EEGPAS,'C3'), -0.015);
ylim([-40 40]);
legend off;
subplot(122)
plotPCTEP(EEGPAS, [1 5 10 15 20 30], {'b', 'r', 'g', 'k', ':b', ':r'}, findChanLoc(EEGPAS,'Cz'), -0.015);
ylim([-40 40]);